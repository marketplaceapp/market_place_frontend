import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import { media } from '../../../global/media'
import EmptyData from '../../../components/empty'
 
const SellingChatScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer}  >
                <EmptyData
                    image={media.no_chats}
                    title="You have got no messages so far!"
                    subTitle="As soon as someone sends you a message, it'll start appearing here!"
                    buttonTitle="Start selling"
                    onPress={() => {navigation.navigate('SellStack', {screen: 'AddSell'})}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
})


export default SellingChatScreen
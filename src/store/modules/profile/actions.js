import { Alert } from 'react-native';
import { users } from '../../../global/sampleData';
import AsyncStorage from '@react-native-async-storage/async-storage';

let nextUserId = 0;

export const setAppTheme = params => {
    return {
        type: '@profile/APP_THEME',
        payload: {
            theme_type: params.theme_type,
            theme_value: params.theme_value,
        },
    };
}

export function setAppThemeValue(app_theme_value) {
    return {
        type: '@profile/APP_THEME_VALUE',
        payload: {
            app_theme_value,
        },
    };
}

export function currentUserProfile(current_user_profile) {
    return {
        type: '@profile/CURRENT_USER_PROFILE',
        payload: {
            current_user_profile,
        },
    };
}

export const addUserProfile = params => {
    return {
        type: '@profile/ADD_USER_PROFILE',
        payload: {
            id: ++nextUserId,
            userName: params.userName,
            email: params.email,
            phoneNumber: params.phoneNumber,
            password: params.password,
            confirmPassword: params.confirmPassword,
        },
    };
};


export const updateUserProfile = params => {

    return {
        type: '@profile/UPDATE_USER_PROFILE',
        payload: {
            id: params.id,
            user_name: params.user_name,
            user_icon: params.user_icon,
        },
    };
};


export const deleteUserProfile = id => {
    return {
        type: '@profile/DELETE_USER_PROFILE',
        payload: {
            id
        },
    };
};

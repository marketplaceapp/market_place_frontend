import React from 'react'
import { SafeAreaView, StyleSheet, Text, View,  } from 'react-native';

import { colors } from '../../utils';

const ForgotPassword = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View>
                <Text>ForgotPassword</Text>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    flex: 1,
    alignItems: 'center',
    backgroundColor: colors.bg_color,
})

export default ForgotPassword
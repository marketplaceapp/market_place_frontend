import React, { useState, useEffect, useContext, } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { CustomHeader } from '../../../components/headers'
import Input from '../../../components/input'
import DarkInput from '../../../components/input/DarkInput'
import { PrimaryButton } from '../../../components/button'
import UseLoader from '../../../components/loader/useLoader'
import { ToastContext } from '../../../context/ToastContext'
import { addProduct } from '../../../services/apiRequests'
import Icon from '../../../utils/icons'

import { launchImageLibrary } from 'react-native-image-picker'
import { media } from '../../../global/media'
import { screenWidth } from '../../../global/constants'
import AppText from '../../../components/text'

const AdItemDetailsScreen = (props) => {
    const navigation = props?.navigation

    const [loader, showLoader, hideLoader] = UseLoader();
    const { showToast } = useContext(ToastContext);

    const [productImgs, setProductImgs] = useState([]);

    const [details, setDetails] = useState({});

    const [errors, setErrors] = useState({});
    const [form, setForm] = useState({
        name: '',
        description: '',
        price: '',
        discount: '',
        category: '',
        sub_category: ''
    });

    useEffect(() => {
        const params = props?.route?.params?.params

        const item = params?.item
        const subCategory = params?.sub_category
        console.log("params>> ",params);
        setForm({
            category: item?.title,
            sub_category: subCategory?.title || null
        })
        setDetails(params)
    }, [])
    
    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }

    const chooseFile = () => {
        setErrors({})
        let options = {
            includeBase64: true,
            mediaType: 'photo',
            maxWidth: 300,
            maxHeight: 550,
            quality: 1,
            selectionLimit: 1,
            
        };
        launchImageLibrary(options, (response) => {

        if (response.didCancel) {
            //alert('User cancelled camera picker');
            return;
        } else if (response.errorCode == 'camera_unavailable') {
            //alert('Camera not available on device');
            return;
        } else if (response.errorCode == 'permission') {
            //alert('Permission not satisfied');
            return;
        } else if (response.errorCode == 'others') {
            //alert(response.errorMessage);
            return;
        }
     
            //onChangeProductDetails({name: 'images', value: response?.assets,}); 

            setProductImgs(response?.assets)
            
            // console.log('====================================');
            // setFilePath(response?.assets[0]?.uri);
            // setMainImage(response?.assets[0]?.uri)
            // setFileType(response?.assets[0]?.type)
        });
    };



    const checkForValidations = () => {
        var isValid = true;

        if(!productImgs || productImgs?.length == 0){
            console.log("Please select images!");
            isValid = false
            
            setErrors((prev) => {
                return {...prev, images: 'Please select images!'}
            })
        }
        
        if(!form?.name){
            console.log("Please select a valid name!");
            isValid = false
            setErrors((prev) => {
                return {...prev, name: 'Please select a valid name!'}
            })
        }

        if(!form?.description){
            console.log("Please select a valid description!");
            isValid = false
            setErrors((prev) => {
                return {...prev, description: 'Please select a valid description!'}
            })
        }

        if(!form?.price){
            console.log("Please select a valid price!");
            isValid = false
            setErrors((prev) => {
                return {...prev, price: 'Please select a valid price!'}
            })
        }

        if(!form?.discount){
            console.log("Please select a valid phone discount!");
            isValid = false
            setErrors((prev) => {
                return {...prev, discount: 'Please select a valid phone discount!'}
            })
        }

        return isValid;
    }

    const saveDetailsFunction = () => {
        console.log("form-> > ", form);

        showLoader();

        if(checkForValidations()){

            let formData = new FormData();
            const newFile = {
                uri: productImgs[0]?.uri,
                type: productImgs[0]?.type,
                name: productImgs[0]?.fileName,
            }
            formData.append('image', newFile)
            formData.append("name", form?.name);
            formData.append("description", form?.description);
            formData.append("price", form?.price);
            formData.append("discount_amount", form?.discount);
            formData.append("category", form?.category);
            formData.append("sub_category", form?.sub_category);
           
            createAdsFunction(formData)
        }else{
            hideLoader()
        }
    }

    const createAdsFunction = async (body) => {
        const onSuccess = (resp) => {
            console.log("add product resp > ",resp);
            hideLoader()
            showToast(resp?.message, true);
            navigation.navigate('MyAdsStack', {screen: 'MyAdsScreen'})
        };

        const onError = (err) => {
            console.log("add product err > ",err?.response);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        addProduct(
            body,
            onSuccess,
            onError
        );
    };


    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader
                title="Ads Detail"
                isBack={true}
            />
            <ScrollView>
                <View style={styles.mainContainer}  >
                    <View style={{marginBottom: 15}}>
                        {productImgs && productImgs?.length != 0
                        ?
                        <View style={{alignItems: 'center', justifyContent: 'center'}} >
                            <Image source={{uri: productImgs[0].uri}} style={{height: 300, width: screenWidth, }} />
                            <View style={{position: 'absolute', alignItems: 'center', justifyContent: 'center'}} >
                                <TouchableOpacity 
                                    onPress={() => {chooseFile()}}
                                    style={{marginTop: 20, height: 40, flexDirection: 'row',  backgroundColor: colors.primary, alignItems: 'center', justifyContent: 'center', padding: 10, borderRadius: 4,}} >
                                    <Icon type="AntDesign" name="edit" size={20} color={colors.white} />
                                    <Text style={{color: colors.white, fontSize: 16, marginLeft: 8 }} >Edit Images</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                        :
                        <View style={{alignItems: 'center', justifyContent: 'center'}} >
                            <Image source={media.admin} style={{height: 70, width: 70, }} /> 
                            <TouchableOpacity
                                onPress={() => {chooseFile()}}
                                style={{marginTop: 20, height: 40, flexDirection: 'row',  backgroundColor: colors.primary, alignItems: 'center', justifyContent: 'center', padding: 10, borderRadius: 4,}} >
                                <Icon type="AntDesign" name="plus" size={20} color={colors.white} />
                                <Text style={{color: colors.white, fontSize: 16, marginLeft: 4 }} >Add Images</Text>
                            </TouchableOpacity>
                        </View>
                        }
                        <View style={{alignItems: 'center',}}>
                            {errors?.images && 
                                <AppText
                                    text={errors?.images}
                                    type={STRING_CONSTANTS.textConstants.BODY}
                                    customStyle={{color: colors.red, textAlign: 'center'}}
                                />
                                }
                        </View>
                    </View>
            
                <Input
                    label="Product Name"
                    placeholder="Enter product name"
                    value={form?.name}
                    error={errors?.name}
                    onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                />

                <Input
                    label="Description"
                    placeholder="Enter product description"
                    value={form?.description}
                    error={errors?.description}
                    onChangeText={(text) => {onChange({name: 'description', value: text,}); setErrors({}); }}
                />

                <Input
                    label="Price"
                    isPhoneNumber
                    placeholder="Enter product price"
                    value={form?.price}
                    error={errors?.price}
                    onChangeText={(text) => {onChange({name: 'price', value: text,}); setErrors({}); }}
                />
                
                <Input
                    label="Discount"
                    isPhoneNumber
                    placeholder="Enter product discount"
                    value={form?.discount}
                    error={errors?.discount}
                    onChangeText={(text) => {onChange({name: 'discount', value: text,}); setErrors({}); }}
                />
                
                <Input
                    label="Category"
                    placeholder="Enter product category"
                    value={form?.category}
                    editable={false}
                />

                {details?.sub_category && (
                    <Input
                        label="Sub Category"
                        placeholder="Enter product sub category"
                        value={form?.sub_category}
                        editable={false}
                    />
                )}

                <PrimaryButton
                    title="Save Details"
                    buttonLoader={loader}
                    onPress={() => {saveDetailsFunction()}}
                />
            </View>
            </ScrollView>
          
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 10,
    }
})

export default AdItemDetailsScreen
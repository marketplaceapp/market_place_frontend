import React, { useState, useEffect, useContext } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import Categories from '../../../components/custom/home/Categories'
import { categories, products } from '../../../global/sampleData'
import ProductList from '../../../components/custom/home/ProductList'
import { ToastContext } from '../../../context/ToastContext'
import { deleteProduct, getProductsById } from '../../../services/apiRequests'
import Loader from '../../../components/loader'
import { media } from '../../../global/media'
import AppText from '../../../components/text'
import { BackButton, PrimaryButton } from '../../../components/button'
import CustomIcon from '../../../components/CustomIcon'
import { addITemToCart, deleteItemFromCart } from '../../../store/modules/order/actions'
import { useDispatch, useSelector } from 'react-redux'
import UseLoader from '../../../components/loader/useLoader'
 
const ProductDetailsScreen = (props) => {
    const dispatch = useDispatch()
    const navigation = props?.navigation
    
    const cart_list = useSelector(state => state.order.cart_list);
    //console.log("cart_list=> ",cart_list);

    const [loader, showLoader, hideLoader] = UseLoader();
    const [fromWhere, setFromWhere] = useState(null);

    const [loading, setLoading] = useState(false);
    const [productDetails, setProductDetails] = useState([]);
    const [existInCart, setExistInCart] = useState(false);
    const [cartItems, setCartItems] = useState(cart_list);
    const { showToast } = useContext(ToastContext);

    useEffect(() => {
        const details = props?.route?.params?.params

        const from = props?.route?.params?.fromScreen ?  props?.route?.params?.fromScreen : null
        setFromWhere(from)
        setProductDetails(details)
        //fetchData(details?.id)
        //console.log("details>> ",details);

    }, [])



    useEffect(() => {
        console.log("cart_list> > .... ",cart_list);
        let magenicVendors = cart_list && cart_list?.filter( item => item?.id == productDetails?.id )
        console.log("magenicVendors> >  ",magenicVendors);
        setExistInCart(magenicVendors?.length > 0 ? true : false)
    }, [cart_list])



    const fetchData = async (id) => {
        const onSuccess = (resp) => {
            console.log("product details resp > ",resp?.data);
            setLoading(false);
            setProductDetails(resp?.data)
        };

        const onError = (err) => {
            console.log("product details err > ",err?.response?.data);
            setLoading(false);
            showToast(err?.response?.data?.message, false);
        };

        getProductsById(
            id,
            onSuccess,
            onError
        );
    };

    const deleteDetailsFromCart = () => {
        showLoader()
        dispatch(deleteItemFromCart(productDetails?.id))

        setTimeout(() => {
            hideLoader()
        }, 1000);
    }

    const addDetailsToCart = () => {
        showLoader()
        dispatch(addITemToCart({
            id: productDetails.id,
            name: productDetails.name,
            description: productDetails.description,
            price: productDetails.price,
            image: productDetails.image,
            discount_amount: productDetails.discount_amount,
            category: productDetails.category,
            sub_category: productDetails.sub_category,
        }))

        setTimeout(() => {
            hideLoader()
        }, 1000);
    }



    const deleteProductFunction = (id) => {
        showLoader()
        const onSuccess = (resp) => {
            console.log("delete product resp > ",resp);
            hideLoader()
            showToast(resp?.message, true);
            navigation.goBack()
        };

        const onError = (err) => {
            console.log("delete product err > ",err?.response);
            hideLoader()
            showToast(err?.response?.data?.message, false);
        };

        deleteProduct(
            id,
            onSuccess,
            onError
        );
    };

    if(loading) return <Loader loaderColor={colors.primary} />
    return (
        <SafeAreaView style={styles.container} >
              
            <View style={styles.mainContainer}>
                <View>
                   
                    <Image source={productDetails?.image ? {uri : productDetails?.image} : media.admin} style={styles.cardImage} />
                    <View style={styles.cardContent} >
                        <AppText
                            text={`Rs. ${productDetails?.price}`}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{}}
                            numberOfLines={1}
                        />
                        <AppText
                            text={productDetails?.name}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{}}
                            numberOfLines={1}
                        />
                        <AppText
                            text={productDetails?.description}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{color: colors.gray}}
                            numberOfLines={2}
                        />
                        <AppText
                            text={productDetails?.discount_amount}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{color: colors.gray}}
                            numberOfLines={2}
                        />
                        <AppText
                            text={productDetails?.category}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{color: colors.gray}}
                            numberOfLines={2}
                        />
                        <AppText
                            text={productDetails?.sub_category}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{color: colors.gray}}
                            numberOfLines={2}
                        />


                        {productDetails?.status == 'sold' && (
                                <View style={{marginTop: 20, height: 40, width: '100%', alignItems: 'center', justifyContent: 'center', borderRadius: 4, backgroundColor: colors.reddish}} >
                                    <AppText
                                        text={'Sold'}
                                        type={STRING_CONSTANTS.textConstants.BODY}
                                        customStyle={{marginTop: 2, marginRight: 4, color: colors.white}}
                                        numberOfLines={1}
                                    />
                                </View>
                            )}
                            
                    </View>
                </View>

                <View>
                    {fromWhere != 'My_Ads' ? (
                        <View style={{padding: 20, paddingBottom: 0}} >
                            {existInCart 
                            ? 
                            <PrimaryButton
                                title="Remove from Cart"
                                buttonLoader={loader}
                                onPress={() => {
                                    deleteDetailsFromCart()
                                }}
                            />
                            :
                            <PrimaryButton
                                title="Add To Cart"
                                buttonLoader={loader}
                                onPress={() => {
                                    addDetailsToCart()
                                }}
                            />
                            }
                            
                        </View>
                    ) : (
                        <View style={{padding: 20, paddingBottom: 0}} >
                            <PrimaryButton
                                title="Delete"
                                buttonLoader={loader}
                                onPress={() => {
                                    deleteProductFunction(productDetails?.id)
                                }}
                            />
                        </View>
                    )}
                </View>

                
                <View style={{position: 'absolute', top: 10, left: 10, }} >
                    <TouchableOpacity
                        activeOpacity={0.5}
                        onPress={() => {navigation.goBack()}}
                        style={[styles.buttonCntainer, {} ]}
                        >

                         <CustomIcon
                            iconName="arrowleft"
                            iconType="AntDesign"
                            iconColor={colors.black}
                            iconContainer={{}}
                            iconSize={24}
                            onPress={() => {navigation.goBack()}}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'space-between'
        //padding: 20,
    },
    cardImage: {
        height: 240,
        width: '100%',
        //resizeMode: 'contain'
    },
    cardContent: {
        padding: 20,
    },
    buttonCntainer: {
        height: 40,
        width: 40,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 20,
        backgroundColor: colors.light_gray
    }
})

export default ProductDetailsScreen
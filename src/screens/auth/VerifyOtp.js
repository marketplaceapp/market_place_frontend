import React, { useRef, useState } from 'react';
import { View, Text, TextInput, Button, StyleSheet, SafeAreaView, Alert } from 'react-native';

import { STRING_CONSTANTS, colors } from '../../utils';

import AppText from '../../components/text';
import { PrimaryButton, TextButton } from '../../components/button';
import { CustomHeader } from '../../components/headers';

const VerifyOtp = ({navigation}) => {

    const [otp, setOtp] = useState('');
    const refs = useRef([]);
  
    const handleOTPChange = (index, value) => {
        const newOtp = otp.split('');
        newOtp[index] = value;
        setOtp(newOtp.join(''));
    
        if (value && index < 5) {
            refs.current[index + 1].focus();
        }
    };
  
    const handleVerifyOTP = () => {
      const correctOTP = '123456'; 
      
      if (otp === correctOTP) {
        Alert.alert('Success', 'OTP verified successfully');
      } else {
        Alert.alert('Error', 'Invalid OTP. Please try again.');
      }
    };
  
    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader isBack />

            <View>
                <AppText
                    text="Verify Code"
                    type={STRING_CONSTANTS.textConstants.HEADING}
                    customStyle={{textAlign: 'center'}}
                />
                <AppText
                    text="Please verify the code sent to you!"
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{textAlign: 'center', paddingHorizontal: 40, marginVertical: 10}}
                />
            </View>


            <View style={{alignItems: 'center', width: '100%', padding: 20}} >

                <View style={styles.otpContainer} >
                    {Array.from({ length: 6 }).map((_, index) => (
                        <TextInput
                            key={index}
                            style={styles.input}
                            ref={ref => (refs.current[index] = ref)}
                            maxLength={1}
                            keyboardType="numeric"
                            value={otp[index] || ''}
                            onChangeText={value => handleOTPChange(index, value)}
                        />
                    ))}
                </View>
                
                <AppText
                    text="Didn't recieved OTP?"
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{textAlign: 'center', color: colors.gray}}
                />
                <TextButton
                    title="Resend Code"
                    onPress={() => {}}
                />

                <PrimaryButton
                    title="Verify OTP" 
                    //buttonLoader={loader}
                    buttonStyle={{marginTop: 30}}
                    onPress={() => {
                        handleVerifyOTP()
                    }} 
                />

            </View>

            
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        paddingHorizontal: 20,
    },
    otpContainer: {
        marginBottom: 30,
        flexDirection: 'row',
        alignItems: 'center'
    },
    title: {
        fontSize: 24,
        fontWeight: 'bold',
        marginBottom: 20,
    },
    input: {
        width: 40,
        height: 40,
        borderWidth: 1,
        borderColor: '#ccc',
        borderRadius: 5,
        paddingHorizontal: 10,
        marginHorizontal: 5,
        alignItems: 'center',
        justifyContent: 'center',
        textAlign: 'center'
    },
});


export default VerifyOtp

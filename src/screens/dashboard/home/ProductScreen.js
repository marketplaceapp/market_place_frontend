import React, { useState, useEffect, useContext } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import Categories from '../../../components/custom/home/Categories'
import { categories, products } from '../../../global/sampleData'
import ProductList from '../../../components/custom/home/ProductList'
import { ToastContext } from '../../../context/ToastContext'
import { getProducts } from '../../../services/apiRequests'
import Loader from '../../../components/loader'
import EmptyData from '../../../components/empty'
import { media } from '../../../global/media'
import { CustomHeader } from '../../../components/headers'
 
const ProductScreen = ({navigation}) => {


    const [loading, setLoading] = useState(true);
    const [productsArr, setProductsArr] = useState([]);
    const [originalArr, setOriginalArr] = useState([]);

    const { showToast } = useContext(ToastContext);

    useEffect(() => {
        fetchData()
    }, [])


    const fetchData = async (e) => {
        const onSuccess = (resp) => {
            // console.log("products resp > ",resp?.data);
            setLoading(false);
            setProductsArr(resp?.data)
            setOriginalArr(resp?.data)
        };

        const onError = (err) => {
            console.log("products err > ",err?.response?.data);
            setLoading(false);
            showToast(err?.response?.data?.message, false);
        };

        getProducts(
            onSuccess,
            onError
        );
    };

    const viewProductDetailFunc = (val) => {
        console.log("val> > ",val);
        navigation.navigate('ProductDetails', {params: val})
    }

    if(loading) return <Loader loaderColor={colors.primary} />
    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader
                title="Products"
                isBack={true}
            />  
            <View style={styles.mainContainer}>
                
                {productsArr && productsArr?.length != 0 ? (
                        <ProductList
                            data={productsArr}
                            viewProductDetails={(val) => viewProductDetailFunc(val)}
                        />
                    ):(
                        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}} >
                            <EmptyData
                                image={media.no_chats}
                                title="You have got no items so far!"
                                subTitle="As soon as someone add new post, it'll start appearing here!"
                                buttonTitle="Start selling"
                                onPress={() => {navigation.navigate('SellStack', {screen: 'AddSell'})}}
                            />
                        </View>
                    )}
                    
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
    }
})

export default ProductScreen
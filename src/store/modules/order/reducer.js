
import { 
    CURRENT_CUSTOMER_DETAILS, 
    SET_CHARGES_DETAILS, 
    SET_DELIVERY_DETAILS, 
    SET_OUTFIT_DETAILS, 
    SET_MEASUREMENT_DETAILS,
    SET_CLOTH_IMAGES,
    ADD_ITEM_TO_CART,
    REMOVE_ITEM_FROM_CART,
    CLEAR_CART_LIST,
} from "./actionTypes";

const INITIAL_STATE = {
    cart_list: [],


    current_customer_details: null,
    
    outfit_details: null,
    measurement_details: null,
    delivery_details: null,
    charges_details: null,

    cloth_images: null,
};

export default function order(state = INITIAL_STATE, action) {
    switch (action.type) {

        case ADD_ITEM_TO_CART: {
            const {
                id, name, description, price, image, discount_amount, category, sub_category,
            } = action.payload
            return {
                ...state,
                cart_list: [
                    ...state.cart_list, {
                        id, name, description, price, image, discount_amount, category, sub_category,
                    }
                ]
            }
        };

        case REMOVE_ITEM_FROM_CART: {
            const { id } = action.payload
                return {
                    ...state,
                    cart_list: state.cart_list.filter((item) => item.id != id)
            };
        }

        case CLEAR_CART_LIST: {
            const {  } = action.payload
                return {
                    ...state,
                    cart_list: []
            };
        }






        case CURRENT_CUSTOMER_DETAILS: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                current_customer_details: action.payload
            }
        };

        case SET_OUTFIT_DETAILS: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                outfit_details: action.payload
            }
        };

        case SET_MEASUREMENT_DETAILS: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                measurement_details: action.payload
            }
        };

        case SET_DELIVERY_DETAILS: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                delivery_details: action.payload
            }
        };

        case SET_CHARGES_DETAILS: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                charges_details: action.payload
            }
        };

        case SET_CLOTH_IMAGES: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                cloth_images: action.payload
            }
        };

        default:
        return state;
    }
}

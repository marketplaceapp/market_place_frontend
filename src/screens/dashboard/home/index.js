import React, { useState, useEffect, useContext } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, ScrollView } from 'react-native'
import { colors } from '../../../utils'
import HomeHeader from '../../../components/headers/HomeHeader'
import ProductCard from '../../../components/custom/home/ProductCard'
import { products, categories } from '../../../global/sampleData'
import Categories from '../../../components/custom/home/Categories'
import UseLoader from '../../../components/loader/useLoader'
import { ToastContext } from '../../../context/ToastContext'
import { getProducts } from '../../../services/apiRequests'
import Loader from '../../../components/loader'
import { useSelector } from 'react-redux'
import EmptyData from '../../../components/empty'
import { media } from '../../../global/media'
 
const HomeScreen = ({navigation}) => {

        
    const cart_list = useSelector(state => state.order.cart_list);

    const [loading, setLoading] = useState(true);
    const [productsArr, setProductsArr] = useState([]);
    const [originalArr, setOriginalArr] = useState([]);

    const { showToast } = useContext(ToastContext);

    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            console.log("home screen > > ");
            setLoading(true)
            fetchData()
        });
    
        return unsubscribe;
    }, [navigation]);
 

    const fetchData = async (e) => {
        const onSuccess = (resp) => {
            // console.log("products resp > ",resp?.data);
            setLoading(false);
            setProductsArr(resp?.data)
            setOriginalArr(resp?.data)
        };

        const onError = (err) => {
            console.log("products err > ",err?.response?.data);
            setLoading(false);
            showToast(err?.response?.data?.message, false);
        };

        getProducts(
            onSuccess,
            onError
        );
    };

    const viewSubCategory = (val) => {
        console.log("val> > ",val);
        //navigation.navigate('SubCategories')

        if(val?.sub_category){
            navigation.navigate('SubCategories', {params: val})
        }else{
            navigation.navigate('Products', {params: val})
        }
    }

    const viewProductDetailFunc = (val) => {
        console.log("val> > ",val);
        navigation.navigate('ProductDetails', {params: val})
    }


    if(loading) return <Loader loaderColor={colors.primary}/>
    return (
        <SafeAreaView style={styles.container} >
            <HomeHeader cartItems={cart_list?.length} />
            <ScrollView>
                <View style={styles.mainContainer}>
                    <Categories 
                        title="Browse Categories" 
                        data={categories} 
                        horizontal={true}
                        onViewSubCat={(val) => {viewSubCategory(val)}}
                        onViewAll={() => {navigation.navigate('Categories')}} 
                    />

                    {productsArr && productsArr?.length != 0 ? (
                        <ProductCard 
                            title="Fresh recommendations" 
                            data={productsArr}
                            viewProductDetails={(val) => {viewProductDetailFunc(val)}}
                        />
                    ):(
                        <View style={{flex: 1, marginTop: 100, alignItems: 'center', justifyContent: 'center'}} >
                            <EmptyData
                                image={media.no_chats}
                                title="You have got no items so far!"
                                subTitle="As soon as someone add new post, it'll start appearing here!"
                                buttonTitle="Start selling"
                                onPress={() => {navigation.navigate('SellStack', {screen: 'AddSell'})}}
                            />
                        </View>
                    )}
                    
                </View>
            </ScrollView>

          
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 0,
    }
})

export default HomeScreen
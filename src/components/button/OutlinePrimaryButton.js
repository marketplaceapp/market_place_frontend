import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { screenWidth } from '../../global/constants'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import Loader from '../loader'
 

const OutlinePrimaryButton = ({title, disabled, buttonLoader, loaderColor, onPress}) => {

    return (
        <View style={styles.container} >
            {title != 'Delete'
            ?
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.button, {}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader loaderColor={loaderColor} /> : <Text style={styles.title} >{title}</Text>}
            </TouchableOpacity>
            :
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.deleteButton, {backgroundColor: disabled ? colors.gray : colors.white}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader loaderColor={colors.red} /> : <Text style={styles.deleteTitle} >{title}</Text>}
            </TouchableOpacity>
            }
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 14,
    },
    button: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: 0.5,
    },
    deleteButton: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        borderWidth: 1,
        borderColor: colors.red,
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.black,
    },
    deleteTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.red,
    }
})

export default OutlinePrimaryButton
import { STRING_CONSTANTS } from "../utils/stringConstants";
import { media } from "./media";



export const onboarding_data = [
    {
        id: 1,
        image: media.onboarding_1,
        title: 'Seamless Shopping Experience',
        description: 'Enjoy a seamless shopping journey with our intuitive app interface',
    },
    {
        id: 2,
        image: media.onboarding_2,
        title: 'Wishlist: Where Fashion Dreams Begin',
        description: 'Create your wishlist and let your fashion dreams take fligh.'
    },
    {
        id: 3,
        image: media.onboarding_3,
        title: 'Swift and Reliable Delivery',
        description: 'Experience swift and reliable delivery right to your doorstep'
    },
]


export const wishlist_categories = [
    {
        title: 'All',
    },
    {
        title: 'Shirts',
    },
    {
        title: 'Pants',
    },
    {
        title: 'Coats',
    },
]

export const wishlist_products = [
    {
        title: "Men's Classic Coat",
        description: 'lorem epsumn lorem epsumn lorem epsumnlorem epsumnlorem epsumn',
        image: '',
        price: '200',
    },
    {
        title: "Men's Classic Coat",
        description: 'lorem epsumn lorem epsumn lorem epsumnlorem epsumnlorem epsumn',
        image: '',
        price: '200',
    },
    {
        title: "Men's Classic Coat",
        description: 'lorem epsumn lorem epsumn lorem epsumnlorem epsumnlorem epsumn',
        image: '',
        price: '200',
    },
    {
        title: "Men's Classic Coat",
        description: 'lorem epsumn lorem epsumn lorem epsumnlorem epsumnlorem epsumn',
        image: '',
        price: '200',
    },
    {
        title: "Men's Classic Coat",
        description: 'lorem epsumn lorem epsumn lorem epsumnlorem epsumnlorem epsumn',
        image: '',
        price: '200',
    },
]

export const categories = [
    {
        title: 'Cars',
        icon: media.car,
    },
    {
        title: 'Properties',
        icon: media.building,
        sub_category: [
            {
                title: 'For Sale: Houses & Apartments'
            },
            {
                title: 'For Rent: Houses & Apartments'
            },
            {
                title: 'Lands & Plots'
            },
            {
                title: 'For Rent: Shops & Offices'
            },
            {
                title: 'For Sale: Shops & Offices'
            },
            {
                title: 'PG & Gest Houses'
            },
        ]
    },
    {
        title: 'Mobiles',
        icon: media.mobiles,
        sub_category: [
            {
                title: 'Mobile Phones'
            },
            {
                title: 'Accessories'
            },
            {
                title: 'Tablets'
            },
        ]
    },
    {
        title: 'Jobs',
        icon: media.jobs,
        sub_category: [
            {
                title: 'Data entry & Back office'
            },
            {
                title: 'Sales & Marketing'
            },
            {
                title: 'BPO & Telecaller'
            },
            {
                title: 'Driver'
            },
            {
                title: 'Office Assistant'
            },
            {
                title: 'Delivery & Collection'
            },
            {
                title: 'Teacher'
            },
            {
                title: 'Cook'
            },
            {
                title: 'Receptionalist & Front Office'
            },
            {
                title: 'Accountant'
            },
            {
                title: 'Other Jobs'
            },
        ]
    },
    {
        title: 'Bikes',
        icon: media.bycicle,
        sub_category: [
            {
                title: 'Motorcycles'
            },
            {
                title: 'Scooters'
            },
            {
                title: 'Spare Parts'
            },
            {
                title: 'Bicycles'
            },
        ]
    },
    {
        title: 'Electronics & Appliances',
        icon: media.electronics,
        sub_category: [
            {
                title: 'TVs, Video - Audio'
            },
            {
                title: 'Kitchen & Other Appliances'
            },
            {
                title: 'Computers & Laptops'
            },
            {
                title: 'Cameras & Lenses'
            },
            {
                title: 'Games & Entertainment'
            },
        ]
    },
    {
        title: 'Commercial Vehicles & Spare Parts',
        icon: media.spareparts,
        sub_category: [
            {
                title: 'Commercial & Other Vehicles'
            },
            {
                title: 'Spare Parts'
            },
        ]
    },
    {
        title: 'Furniture',
        icon: media.furniture,
        sub_category: [
            {
                title: 'Sofa & Dining'
            },
            {
                title: 'Beds & Wardrobes'
            },
        ]
    },
    {
        title: 'Fashion',
        icon: media.clothes,
        sub_category: [
            {
                title: 'Men'
            },
            {
                title: 'Women'
            },
            {
                title: 'Kids'
            },
        ]
    },

    {
        title: 'Books, Sports & Hobbies',
        icon: media.hobbies,
        sub_category: [
            {
                title: 'Books'
            },
            {
                title: 'Gym & Fitness'
            },
            {
                title: 'Other Hobbies'
            },
        ]
    },
    {
        title: 'Pets',
        icon: media.pet,
        sub_category: [
            {
                title: 'Fishes & Aquarium'
            },
            {
                title: 'Pet Food & Accessories'
            },
            {
                title: 'Dog'
            },
            {
                title: 'Other Pets'
            },
        ]
    },
    {
        title: 'Services',
        icon: media.bell,
        sub_category: [
            {
                title: 'Education & Classes'
            },
            {
                title: 'Tours & Travel'
            },
            {
                title: 'Packers & Movers'
            },
            {
                title: 'Other Services'
            },
        ]
    },

]


export const products = [
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
    {
        "_id": "65fdcd4388b32dbfe83fc486",
        "name": "Product 1",
        "description": "Description 1",
        "specifications": [
            "Specs"
        ],
        "image": "http://market-place-tvsh.onrender.com/public/uploads/1715798294440_Voucher.png",
        // "image": "http://nodejs-ecommerce-api-j6vv.onrender.com/public/uploads/1-6-8-6-7-2-4-5-6-9-4-0-6-p-t-e---v-o-u-c-h-e-r-.-j-p-g-1715619054009.jpeg",
        "images": [
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248096.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248308.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248476.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248505.jpg",
            "http://zamtail-backend-r8v1.onrender.com/public/uploads/images-1713519248531.jpg"
        ],
        "price": 100,
        "category": "65fdcce788b32dbfe83fc473",
        "countInStock": 2,
        "product_count": 6,
        "productType": "gents",
        "discount_percentage": 5,
        "discount_amount": 50,
        "offer_price": 50,
        "dateCreated": "2024-03-22T18:26:11.179Z",
        "createdAt": "2024-03-22T18:26:11.181Z",
        "updatedAt": "2024-03-22T18:26:11.181Z",
        "__v": 0,
        "id": "65fdcd4388b32dbfe83fc486"
    },
]
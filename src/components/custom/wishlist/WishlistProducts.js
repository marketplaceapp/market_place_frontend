import React from 'react'
import { Text, View, Image, StyleSheet, FlatList, TouchableOpacity, BackHandler, SafeAreaView  } from 'react-native'
import { wishlist_categories } from '../../../global/sampleData'
import AppText from '../../text'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { screenWidth } from '../../../global/constants'
import { media } from '../../../global/media'
import CustomIcon from '../../CustomIcon'


const WishlistProducts = ({ data, removeProductFunction, moveToBagFunction }) => {

    const Product = ({key, item, name, description, price, image, images }) => {
        return(
            <TouchableOpacity 
                key={key} 
                style={[styles.productContainer, ]} 
                onPress={() => {}}
                >
                    <View style={styles.productImgContainer} >
                        <Image
                            source={image == null ? media.admin : { uri: image }}
                            style={styles.productImage}
                        />
                    </View>
                    <View style={styles.productContentContainer} >
                        <AppText
                            text={name}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{textAlign: 'center',  }}
                        />
                        <AppText
                            text={`Rs. ${price}`}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{textAlign: 'center',  }}
                        />
                    </View>
                    <TouchableOpacity 
                        activeOpacity={0.5}
                        style={styles.moveToBagContainer} 
                        onPress={() => {moveToBagFunction(item)}}    
                    >
                        <AppText
                            text="Move to bag"
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{textAlign: 'center', color: colors.primary }}
                        />
                    </TouchableOpacity>

                    <CustomIcon
                        iconName="close"
                        iconType="AntDesign"
                        iconSize={20}
                        iconColor={colors.black}
                        iconContainer={{position: 'absolute', right: 5, top: 5, height: 30, width: 30, borderRadius: 15, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.light_gray}}
                        onPress={() => {removeProductFunction(item?.id)}}
                    />
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container} >
            <FlatList
                data={data}
                keyExtractor={item => item?.id}
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', flexWrap: 'wrap'}}
                renderItem={({item}) => (
                    <Product 
                        key={item?.id} 
                        item={item} 
                        name={item?.name} 
                        description={item?.description} 
                        price={item?.price} 
                        image={item?.image} 
                        images={item?.images} 
                    />
                )}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        marginVertical: 12,
    },
    productContainer: {
        //height: 300,
        width: screenWidth/2-25,
        borderRadius: 8,
        borderWidth: 0.5,
        marginBottom: 15,
        borderColor: colors.gray,
    },
    productImgContainer: {
        height: 200,
    },
    productContentContainer: {
        padding: 10,
        width: '100%',
        alignItems: 'flex-start'
    },
    productImage: {
        height: '100%',
        width: '100%',
        resizeMode: 'cover',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
    },
    moveToBagContainer: {
        borderTopWidth: 0.5,
        borderColor: colors.gray,
        padding: 10,
    }
})

export default WishlistProducts
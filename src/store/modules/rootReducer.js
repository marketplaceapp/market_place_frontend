import { combineReducers } from 'redux';

import auth from './auth/reducer';
import home from './home/reducer';
import order from './order/reducer';
import profile from './profile/reducer';
import cart from './cart/reducers';

export default combineReducers({
  auth,
  home,
  profile,
  order,
  cart,
});

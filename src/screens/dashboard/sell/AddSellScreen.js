import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import { CustomHeader } from '../../../components/headers'
import Categories from '../../../components/custom/home/Categories'
import { categories } from '../../../global/sampleData'
import SellCategories from '../../../components/custom/home/SellCategories'
 
const AddSellScreen = ({navigation}) => {


    const viewSubCategory = (val) => {
        console.log("val> > ",val);
        if(val?.sub_category){
            navigation.navigate('SellSubCategory', {params: val})
        }else{
            navigation.navigate('AdItemDetails', {params: { item: val, sub_category: '' }} )
        }
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader
                title="What are you offering?"
                isBack={true}
            />
            <View style={styles.mainContainer}  >
                <SellCategories
                    data={categories} 
                    onViewSubCat={(val) => viewSubCategory(val)} 
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        paddingTop: 10,
    }
})

export default AddSellScreen
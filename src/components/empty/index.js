import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, BackHandler, SafeAreaView  } from 'react-native'
import { media } from '../../global/media'
import { STRING_CONSTANTS } from '../../utils'
import AppText from '../text'
import { PrimaryButton } from '../button'


const EmptyData = ({ title, subTitle, buttonTitle, onPress, image }) => {
    return (
        <View style={styles.container} >
            <Image source={image} style={{height: 150, width: 150, }} />
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                customStyle={{marginTop: 4, textAlign: 'center' }}
            />
            <AppText
                text={subTitle}
                type={STRING_CONSTANTS.textConstants.SUBHEADLINE1}
                customStyle={{marginTop: 4, textAlign: 'center' }}
            />

            {buttonTitle && (
                <PrimaryButton
                    title={buttonTitle}
                    buttonStyle={{marginTop: 30}}
                    onPress={onPress} 
                />
            )}

        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        width: '100%',

    }
})

export default EmptyData
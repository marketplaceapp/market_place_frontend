import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { screenWidth } from '../../../global/constants'
import AppText from '../../text'
import { media } from '../../../global/media'
import CustomIcon from '../../CustomIcon'
import { useNavigation } from '@react-navigation/native'
 
const SubCategories = ({ data, title, onViewAll, onCardPress, horizontal }) => {

    const navigation = useNavigation()

    return (
        <View style={styles.container} >
            {title && (<View style={{marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <AppText
                    text={title}
                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                    customStyle={{marginTop: 4, }}
                    numberOfLines={1}
                />
                {onViewAll && (
                <TouchableOpacity 
                    onPress={onViewAll}
                    activeOpacity={0.5}
                >
                    <AppText
                        text={'View all'}
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{marginTop: 4, color: colors.gray, textDecorationLine: 'underline'}}
                        numberOfLines={1}
                    />
                </TouchableOpacity>)}
            </View>)}
            <FlatList
                data={data}
                horizontal={horizontal}
                contentContainerStyle={{ paddingBottom: !horizontal ? 100 : 0 }}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                keyExtractor={item => item?.title}
                renderItem={({item, index}) => {

                    const { title, icon } = item

                    return(
                        <TouchableOpacity 
                            key={title} 
                            style={horizontal ? styles.cardContainer : styles.cardContainerLong} 
                            onPress={() => {onCardPress(item)}}
                        >
                            <View style={{flex: 1, alignItems: 'center', flexDirection: !horizontal ? 'row' : 'column'}} >
                                <View style={!horizontal ? {flex: 1, height: 50, justifyContent: 'center'} : { height: 50,  alignItems: 'center', justifyContent: 'center'}} >
                                    <AppText
                                        text={title}
                                        type={STRING_CONSTANTS.textConstants.BODY}
                                        customStyle={!horizontal ? {marginLeft: 10, } : {marginTop: 4, textAlign: 'center'}}
                                        numberOfLines={!horizontal ? 1 : 2}
                                    />
                                </View>
                            </View>
                            {!horizontal && (
                                <CustomIcon
                                    iconName="chevron-right"
                                    iconType="Feather"
                                    iconColor={colors.black}
                                    iconContainer={{}}
                                    iconSize={24}
                                    //onPress={() => {navigation.navigate('Notifications')}}
                                />
                            )}
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 0,
    },
    cardContainer: {
        borderRadius: 8,
        paddingVertical: 14,
        marginRight: 14,
        maxWidth: 90,
        minWidth: 75,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardContainerLong: {
        flex: 1,
        borderRadius: 8,
        alignItems: 'center',
        justifyContent: 'space-between',
        flexDirection: 'row',
        borderBottomWidth: 1,
        paddingVertical: 6,
        borderColor: colors.light_gray,
    },
    cardContent: {
    },
    cardImg: {
        height: 40, 
        width: 40,
        //resizeMode: 'contain',
    }
})

export default SubCategories
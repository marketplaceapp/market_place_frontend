import React, { useState, useEffect } from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, BackHandler, SafeAreaView  } from 'react-native'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'
import AppText from '../../components/text'
import AlreadyHaveAccount from '../../components/AlreadyHaveAccount'
import { PrimaryButton } from '../../components/button'
import { screenHeight } from '../../global/constants'
import { media } from '../../global/media'
 

const GetStarted = ({navigation}) => {

    useEffect(() => {
        const backAction = () => {
            BackHandler.exitApp();
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );
    
        return () => backHandler.remove();
    }, []);


    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imageContainer} >
                <Image
                    source={media.get_started}
                    style={styles.image}
                />
            </View>
            <View style={styles.contentContainer} >
                <AppText
                    text="Welcome to MarketHub"
                    type={STRING_CONSTANTS.textConstants.HEADING}
                    customStyle={{textAlign: 'center', }}
                />
                <AppText
                    text="Buy and sell anything, anytime, anywhere. Your go-to destination for buying and selling anything you need."
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{textAlign: 'center'}}
                />

                <PrimaryButton
                    title="Let's Get Started"
                    onPress={() => {navigation.navigate('Onboarding')}}
                />
                
                <AlreadyHaveAccount 
                    title="Already have an account?"
                    buttonText="Sign In"
                    onPress={() => {navigation.navigate('Login')}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.white,
    },
    contentContainer: {
       // flex: 1,
        width: '100%',
        height: 260,
        paddingVertical: 20,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    imageContainer: {
        flex: 1,
        width: '100%',
        justifyContent: 'center',
        backgroundColor: colors.white,
    },
    image: {
        width: '100%',
        height: 450,
        resizeMode: 'contain'
    },

})

export default GetStarted
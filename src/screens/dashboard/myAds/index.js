import React, { useState, useEffect, useContext } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import EmptyData from '../../../components/empty'
import { media } from '../../../global/media'
import { getMyProductAds } from '../../../services/apiRequests'
import { ToastContext } from '../../../context/ToastContext'
import Loader from '../../../components/loader'
import ProductCard from '../../../components/custom/home/ProductCard'
 
const MyAdsScreen = ({navigation}) => {

    const [loading, setLoading] = useState(true);
    const [productsArr, setProductsArr] = useState([]);
    const [originalArr, setOriginalArr] = useState([]);

    const { showToast } = useContext(ToastContext);


    useEffect(() => {
        const unsubscribe = navigation.addListener('focus', () => {
            console.log("ads screen > > ");
            setLoading(true)
            fetchData()
        });
    
        return unsubscribe;
    }, [navigation]);
 


    const fetchData = async (e) => {
        console.log('fetchData> ');
        const onSuccess = (resp) => {
            console.log("products resp > ",resp?.data);
            setLoading(false);
            setProductsArr(resp?.data)
            setOriginalArr(resp?.data)
        };

        const onError = (err) => {
            console.log("products err > ",err?.response?.data);
            setLoading(false);
            showToast(err?.response?.data?.message, false);
        };

        getMyProductAds(
            onSuccess,
            onError
        );
    };

    const viewProductDetailFunc = (val) => {
        console.log("val> > ",val);
        navigation.navigate('ProductDetails', {params: val, fromScreen: 'My_Ads'})
    }


    if(loading) return <Loader loaderColor={colors.primary} />
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer}  >
                
                {productsArr && productsArr?.length != 0 ? (
                    <ProductCard
                        data={productsArr}
                        viewProductDetails={(val) => {viewProductDetailFunc(val)}}
                    />
                ):(
                    <EmptyData
                        image={media.no_ads}
                        title="You haven't listed anything yet"
                        subTitle="Let go of what you don't use anymore"
                        buttonTitle="Start Selling"
                        onPress={() => {navigation.navigate('SellStack', {screen: 'AddSell'})}}
                    />
                )}
                
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        width: '100%',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default MyAdsScreen
import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { media } from '../../../global/media'
import AppText from '../../../components/text'
import { PrimaryButton } from '../../../components/button'
import CustomIcon from '../../../components/CustomIcon'
import { getUserFromLocalStorage } from '../../../services/authServices'
 
const ProfileScreen = ({navigation}) => {

    

    const [profile, setProfile] = useState({});

    useEffect(() => {
        getUSer()
    }, [])

    const getUSer = async () => {
        const user = await getUserFromLocalStorage()
        setProfile(user)
    }


    const profileOptions = [
        {
            id: 1,
            title: 'My Network',
            subTitle: 'Followers, following and find friends',
            iconName: 'user-o',
            iconType: 'FontAwesome',
            sub_category: [
                {
                    catTitle: "Buy packages",
                    catSubTitle: "Sell faster, more & higher margins with packages"
                },
                {
                    catTitle: "My Orders",
                    catSubTitle: "Active, scheduled and expired orders"
                },
                {
                    catTitle: "Invoices",
                    catSubTitle: "See and download your invoices"
                },
                {
                    catTitle: "Biiling information",
                    catSubTitle: "Edit your billing name, address, etc."
                },
            ]
        },
        {
            id: 2,
            title: 'Buy Packages & My Orders',
            subTitle: 'Packages, orders, invoices & billing information',
            iconName: 'creditcard',
            iconType: 'AntDesign',
            sub_category: [
                {
                    catTitle: "Buy packages",
                    catSubTitle: "Sell faster, more & higher margins with packages"
                },
                {
                    catTitle: "My Orders",
                    catSubTitle: "Active, scheduled and expired orders"
                },
                {
                    catTitle: "Invoices",
                    catSubTitle: "See and download your invoices"
                },
                {
                    catTitle: "Biiling information",
                    catSubTitle: "Edit your billing name, address, etc."
                },
            ]
        },
        {
            id: 3,
            title: 'Settings',
            subTitle: 'Privacy and logout',
            iconName: 'setting',
            iconType: 'AntDesign',
            sub_category: [
                {
                    catTitle: "Notification",
                    catSubTitle: "Recommendations & Special communications"
                },
                {
                    catTitle: "Communication Preferences",
                    catSubTitle: ""
                },
                {
                    catTitle: "Privacy",
                    catSubTitle: "Change Password"
                },
                {
                    catTitle: "Logout",
                    catSubTitle: ""
                },
                {
                    catTitle: "Logout from all devices",
                    catSubTitle: ""
                },
                {
                    catTitle: "Delete account",
                    catSubTitle: ""
                },
            ]
        },
        {
            id: 4,
            title: 'Help and Support',
            subTitle: 'Help center, Terms and confitions, Privacy policy',
            iconName: 'help-circle',
            iconType: 'Feather',
            sub_category: [
                {
                    catTitle: "Get help",
                    catSubTitle: "See FAQ and contact support"
                },
                {
                    catTitle: "Rate us",
                    catSubTitle: "If you love our app, please take a moment to rate it"
                },
                {
                    catTitle: "Invite friends to App",
                    catSubTitle: "Invite your friends to buy and sell"
                },
                {
                    catTitle: "Version",
                    catSubTitle: "1.0.0(1)"
                },
            ]
        },
    ]


   
    const profileOptionFunction = async (item) => {
        console.log("item=> ", item);
        navigation.navigate('Settings', {params: item})
    }
    

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer}  >
                <View style={{ marginVertical: 12, flexDirection: 'row', alignItems: 'center'}} >
                    <View style={styles.profileImageContainer}>
                        <Image
                            source={profile?.profile_url == null || profile?.profile_url == '' ? media.dummy_avatar : { uri: profile?.profile_url }}
                            style={styles.profileImage}
                        />
                    </View>
                    <View style={{flex: 1, alignItems: 'flex-start'}} >
                        <AppText
                            text={profile?.name}
                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                            customStyle={{textAlign: 'center', color: colors.primary}}
                        />
                        <AppText
                            text={profile?.email}
                            type={STRING_CONSTANTS.textConstants.CAPTION1}
                            customStyle={{textAlign: 'center', color: colors.primary}}
                        />
                    </View>
                </View>

                <PrimaryButton
                    title="View and Edit Profile"
                    onPress={() => {}}
                />

                <View style={{width: '100%', paddingTop: 20}} >
                    {profileOptions.map((item, index) => (
                        <TouchableOpacity 
                            key={index}
                            activeOpacity={0.5}
                            onPress={() => profileOptionFunction(item)}
                            style={[styles.optionContainer, {borderBottomWidth: profileOptions?.length - 1 == index ? 0 : 1.5 }]} >
                            <CustomIcon
                                iconName={item.iconName}
                                iconType={item.iconType}
                                iconColor={colors.black}
                                iconContainer={{width: 40, alignItems: 'center', marginRight: 10}}
                                iconSize={30}
                                //onPress={() => {navigation.navigate('Notifications')}}
                            />
                            <View style={{flex: 1, alignItems: 'flex-start'}} >
                                <AppText
                                    text={item.title}
                                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                                    customStyle={{textAlign: 'left', color: colors.primary}}
                                />
                                <AppText
                                    text={item.subTitle}
                                    type={STRING_CONSTANTS.textConstants.CAPTION2}
                                    customStyle={{textAlign: 'left', color: colors.primary}}
                                />
                            </View>
                            <CustomIcon
                                iconName="chevron-right"
                                iconType="Feather"
                                iconColor={colors.black}
                                iconContainer={{}}
                                iconSize={30}
                                //onPress={() => {navigation.navigate('Notifications')}}
                            />
                        </TouchableOpacity>
                    ))}
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        padding: 20,
    },
    profileContainer: {
        alignItems: 'center',
        paddingVertical: 20,
    },
    profileImageContainer: {
        height: 60,
        width: 60,
        borderRadius: 30,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 14,
    },
    profileImage: {
        height: 60,
        width: 60,
        borderBlockColor: 30,
    },
    optionContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 14,
        borderColor: colors.light_gray
    },
})

export default ProfileScreen
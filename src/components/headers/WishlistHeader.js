import React from 'react'
import { Text, View, StyleSheet } from 'react-native'

import AppText from '../text'
import { STRING_CONSTANTS, colors } from '../../utils'

import CustomIcon from '../CustomIcon'

import { useNavigation } from '@react-navigation/native'

const WishlistHeader = ({title, productCount, onPress, }) => {

    const navigation = useNavigation()


    return (
        <View style={styles.container} >
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.SUBHEADLINE1}
                customStyle={{textAlign: 'center'}}
            />
            <View style={styles.iconContainer} >
                <View>
                    <CustomIcon
                        iconName="close"
                        iconType="AntDesign"
                        iconColor={colors.black}
                        iconContainer={{marginLeft: 4}}
                        onPress={() => {navigation.navigate('CartStack')}}
                    />
                    {productCount > 0 && (
                        <View style={{position: 'absolute', right: -10, top: -10, height: 18, width: 18, borderRadius: 9, backgroundColor: colors.primary}} >
                            <AppText
                                text={productCount}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{textAlign: 'center', fontSize: 11}}
                            />
                        </View>
                    )}
                </View>
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        //paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    }
})

export default WishlistHeader
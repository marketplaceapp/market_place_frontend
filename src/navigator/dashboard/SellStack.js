import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/dashboard/home';
import SellScreen from '../../screens/dashboard/sell';
import AddSellScreen from '../../screens/dashboard/sell/AddSellScreen';
import { colors, fonts } from '../../utils';
import SellSubCategoryScreen from '../../screens/dashboard/sell/SellSubCategoryScreen';
import AdItemDetailsScreen from '../../screens/dashboard/sell/AdItemDetails';

const Stack = createStackNavigator();

const SellStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Sell" 
        >
            <Stack.Screen
                name="Sell"
                component={SellScreen}
                options={{
                    headerShown: false,
                    // headerTitle: 'Home',
                    // headerLeft: () => null,
                    // headerTitleAlign: 'center',
                    // headerStyle: {backgroundColor: colors.black},
                    // headerTitleStyle: {color: colors.white, fontSize: fontSize.Title, fontFamily: Poppins.SemiBold},
                }}
            />
            <Stack.Screen
                name="AddSell"
                component={AddSellScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="SellSubCategory"
                component={SellSubCategoryScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="AdItemDetails"
                component={AdItemDetailsScreen}
                options={{
                    headerShown: false,
                }}
            />
        </Stack.Navigator>
    );
}

export default SellStack
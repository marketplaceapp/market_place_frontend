import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { screenWidth } from '../../global/constants'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import Loader from '../loader'
 

const CustomButton = ({title, disabled, buttonLoader, onPress}) => {
    return (
        <View style={styles.container} >
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.button, {backgroundColor: disabled ? colors.gray : colors.primary}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader size={'small'} /> : <Text style={styles.title} >{title}</Text>}
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        // width: '100%',
        marginVertical: 14,
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        paddingVertical: 10,
        paddingHorizontal: 20,
        height: 50,
        backgroundColor: colors.white,
    },
    title: {
        fontSize: 16,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    }
})

export default CustomButton
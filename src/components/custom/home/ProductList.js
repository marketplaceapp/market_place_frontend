import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { screenWidth } from '../../../global/constants'
import AppText from '../../text'
import { media } from '../../../global/media'
import CustomIcon from '../../CustomIcon'
 
const ProductList = ({ data, viewProductDetails, title, onViewAll  }) => {
    return (
        <View style={styles.container} >

            <FlatList
                data={data}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item?.name}
                renderItem={({item, index}) => {

                    const { id, name, price, image, description, featured = true, date = "12 May", } = item

                    return(
                        <TouchableOpacity 
                            onPress={() => viewProductDetails(item)}
                            key={id} style={styles.cardContainer} >
                            <Image source={{uri: image}} style={styles.cardImg} />
                            <View style={{flex: 1, margin: 14, justifyContent: 'center'}} >
                                <View style={{ flexDirection: 'row', justifyContent: 'space-between',}} >
                                    <View>
                                        {featured && (
                                            <View style={{flexDirection: 'row', alignItems: 'center', padding: 6, paddingVertical: 0, borderRadius: 4, backgroundColor: '#f5a111'}} >
                                                <CustomIcon
                                                    iconName="lightning-bolt"
                                                    iconType="MaterialCommunityIcons"
                                                    iconColor={colors.black}
                                                    iconContainer={{}}
                                                    iconSize={18}
                                                    //onPress={() => {navigation.navigate('Notifications')}}
                                                />
                                                <AppText
                                                    text={'Featured'}
                                                    type={STRING_CONSTANTS.textConstants.BODY}
                                                    customStyle={{marginTop: 2, marginRight: 4,}}
                                                    numberOfLines={1}
                                                />
                                            </View>
                                        )}
                                        <AppText
                                            text={`Rs. ${price}`}
                                            type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                            customStyle={{}}
                                            numberOfLines={1}
                                        />
                                    </View>
                                    <TouchableOpacity
                                        activeOpacity={0.5}
                                        onPress={() => {}}
                                        style={{ height: 28, width: 28, borderRadius: 14, backgroundColor: '#0F172A20', alignItems: 'center', justifyContent: 'center'}}
                                    >
                                        <Image source={true ? media.heart_white : media.heart_white_fill} style={true ? {height: 22, width: 22, resizeMode: 'contain'} : {height: 16, width: 16, resizeMode: 'contain'}} />
                                    </TouchableOpacity>
                                </View>

                                <View>
                                    <View>
                                            <AppText
                                            text={description}
                                            type={STRING_CONSTANTS.textConstants.BODY}
                                            customStyle={{}}
                                            numberOfLines={1}
                                        />
                                    </View>

                                    <View style={{ flexDirection: 'row', justifyContent: 'space-between',}} >
                                        
                                        <AppText
                                            text={date}
                                            type={STRING_CONSTANTS.textConstants.BODY}
                                            customStyle={{color: colors.gray}}
                                            numberOfLines={1}
                                        />
                                    </View>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    cardContainer: {
        flex: 1,
        borderWidth: 1.5,
        borderColor: colors.light_gray,
        backgroundColor: colors.white,
        marginBottom: 12,
        borderRadius: 8,
        width: '100%',
        flexDirection: 'row',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 0.27,
        elevation: 4,
    },
    cardContent: {
       padding: 10,
    },
    cardImg: {
        height: '100%', 
        width: 120, 
        //resizeMode: 'contain',
        borderRadius: 8,
        backgroundColor: colors.light_gray,
    }
})

export default ProductList
import { Dimensions } from "react-native"


export const screenWidth = Dimensions.get('window').width
export const screenHeight = Dimensions.get('window').height


export const random_number = () => {

    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
  
    return (
      S4() +  S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()
    );
}

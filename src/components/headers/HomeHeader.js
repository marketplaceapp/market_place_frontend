import React from 'react'
import { Text, View, StyleSheet, Image } from 'react-native'
import { BackButton } from '../button'
import AppText from '../text'
import { STRING_CONSTANTS, colors } from '../../utils'
import CustomIcon from '../CustomIcon'
import { useNavigation } from '@react-navigation/native'
import { media } from '../../global/media'

const HomeHeader = ({title, onPress, cartItems }) => {

    const navigation = useNavigation()

    const profile = {}

    return (
        <View style={styles.container} >
            <View style={styles.profileImageContainer}>
                <Image
                    source={profile?.image == null ? media.dummy_avatar : { uri: profile?.image }}
                    style={styles.profileImage}
                />
            </View>
            <View style={styles.iconContainer} >
                <View>
                    <CustomIcon
                        iconName="shopping-bag"
                        iconType="FontAwesome"
                        iconColor={colors.black}
                        iconContainer={{marginLeft: 4,}}
                        onPress={() => {navigation.navigate('Cart')}}
                    />
                    {cartItems > 0  && (<View style={{ position: 'absolute', top: -10, right: -10, height: 20, width: 20, borderRadius: 10, alignItems: 'center', justifyContent: 'center', backgroundColor: colors.white, borderWidth: 1.5, borderColor: colors.primary}} >
                        <Text style={{color: colors.primary, fontSize: 10}} >{cartItems}</Text>
                    </View>)}
                </View>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 70,
        width: '100%',
        paddingHorizontal: 20,
        paddingVertical: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    iconContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    profileImageContainer: {
        height: 50,
        width: 50,
        borderRadius: 25,
        borderWidth: 1,
        alignItems: 'center',
        justifyContent: 'center',
        marginRight: 14,
    },
    profileImage: {
        height: 50,
        width: 50,
        borderBlockColor: 25,
    },
})

export default HomeHeader
import axios from 'axios';
import { apiHeaders } from './apiConstants';
import { endpoints } from '../utils/constants/apiConstants';
import { getUserFromLocalStorage } from './authServices';

console.log("URL > >> > > ",endpoints.base_url);
if (__DEV__) {
    console.log('Development');
} else {
    console.log('Production');
}
const authFetch = axios.create({
  baseURL: endpoints.base_url,
  headers: {
    Accept: apiHeaders.acceptType,
    // [apiHeaders.deviceType]: apiHeaders.web,
    // [apiHeaders.language]: Cookies.get('selectedLanguage') || 'en',
    // [apiHeaders.appVersion]: '1.0.0',
    // [apiHeaders.deviceDetails]: browserData,
  },
  timeout: 15000,
});

authFetch.interceptors.request.use(
  (config) => {
    // const deviceUdid = getDeviceIdFromLocalStorage();
    // config.headers[apiHeaders.deviceUdid] = deviceUdid;
    // config.headers[apiHeaders.today] = new Date().toISOString();
    // config.headers[apiHeaders.timezone] =
    //   Intl.DateTimeFormat().resolvedOptions().timeZone;
    return config;
  },
  (error) => {
    return Promise.reject(error);
  }
);

authFetch.interceptors.response.use(
  (response) => {
    return response;
  },
  (error) => {
    console.log("API ERROR >> > ",error);
    if (error?.response?.status === 401) {
        console.log('network error >>> 401');
    }
    if (error?.code === 'ERR_NETWORK') {
      console.log('network error');
    }

    if (error?.response?.status === 404) {
      console.log('not found');
    }
    if (error?.response?.status === 400) {
      console.log('bad request');
    }
    console.log(error?.response?.data?.error_message);
    console.log(error?.response?.data?.error);
    console.log(error);

    return Promise.reject(error);
  }
);

export default authFetch;

import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { screenWidth } from '../../../global/constants'
import AppText from '../../text'
import { media } from '../../../global/media'
import CustomIcon from '../../CustomIcon'
import { useNavigation } from '@react-navigation/native'
 
const SellCategories = ({ data, title, onViewAll, onViewSubCat, horizontal }) => {

    const navigation = useNavigation()


    return (
        <View style={styles.container} >
            <FlatList
                data={data}
                showsVerticalScrollIndicator={false}
                showsHorizontalScrollIndicator={false}
                contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', }}
                keyExtractor={item => item?.title}
                renderItem={({item, index}) => {

                    const { title, icon } = item

                    return(
                        <TouchableOpacity 
                            key={title} 
                            style={[styles.cardContainer, {borderBottomWidth: data?.length % 2 == 0 ? (data?.length - 1 == index || data?.length - 2 == index) ? 0 : 1.5 : data?.length - 1 == index ? 0 : 1.5,  borderRightWidth:  index%2== 0 ? 1.5 : 0, borderColor: colors.light_gray}]} 
                            onPress={() => {onViewSubCat(item)}}
                        >
                            <Image source={icon} style={styles.cardImg} />
                            <View style={{ height: 50,  alignItems: 'center', justifyContent: 'center'}} >
                                <AppText
                                    text={title}
                                    type={STRING_CONSTANTS.textConstants.BODY}
                                    customStyle={{marginTop: 4, textAlign: 'center'}}
                                />
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        marginBottom: 0,
    },
    cardContainer: {
        height: screenWidth/2 -20,
        width: screenWidth/2 -20,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    cardImg: {
        height: 40, 
        width: 40,
        //resizeMode: 'contain',
    }
})

export default SellCategories
import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import LeftArrow from 'react-native-vector-icons/AntDesign'
import { useNavigation } from '@react-navigation/native'

const BackButton = ({ onPress, header}) => {

    const navigation = useNavigation()

    return (
        <>
            <TouchableOpacity
                    activeOpacity={0.5}
                    onPress={() => {navigation.goBack()}}
                    style={[styles.container, {} ]}
                    >
                    <LeftArrow name="arrowleft" color={colors.black} size={22} />
            </TouchableOpacity>
        </>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: colors.gray,
    },
})

export default BackButton

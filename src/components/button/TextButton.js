import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import LeftArrow from 'react-native-vector-icons/AntDesign'
import { useNavigation } from '@react-navigation/native'
import AppText from '../text'
import { STRING_CONSTANTS } from '../../utils'

const TextButton = ({ title, onPress,}) => {

    const navigation = useNavigation()

    return (
        <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}
                style={{}}
            >
                <AppText
                    text={title}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: colors.primary, textDecorationLine: 'underline', }}
                />
        </TouchableOpacity>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 40,
        height: 40,
        borderRadius: 20,
        borderWidth: 1,
        borderColor: colors.gray,
    },
})

export default TextButton

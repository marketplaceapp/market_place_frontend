import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ProfileScreen from '../../screens/dashboard/profile';
import SettingsScreen from '../../screens/dashboard/profile/Settings';

const Stack = createStackNavigator();

const ProfileStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Profile" 
        >
            <Stack.Screen
                name="Profile"
                component={ProfileScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Settings"
                component={SettingsScreen}
                options={{
                    headerShown: false,
                }}
            />
            
        </Stack.Navigator>
    );
}

export default ProfileStack
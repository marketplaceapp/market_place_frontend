import { endpoints } from "../utils/constants/apiConstants";

const baseUrl = endpoints.base_url

// Analytics
// Analytics Count
export function getAnalyticsCount() {
    return(
        fetch(
            `${baseUrl}/${endpoints.analytics_count}`,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}




// Categories
export function getAllCategories() {
    return(
        fetch(
            `${baseUrl}/categories`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}



// Products
export function getAllProducts() {
    return(
        fetch(
            `${baseUrl}/${endpoints.products}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

// Products Count
export function getProductsCount() {
    return(
        fetch(
            `${baseUrl}/${endpoints.products_count}`,
            {
                method: "POST",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}


export function addNewProduct(formData) {
    return(
        fetch(
            // `${baseUrl}/${endpoints.products}`,
            'http://172.20.10.5:8080/api/v1/products',
            {
                method: 'POST',
                headers: {
                    'Accept': "application/json",
                    'Content-Type': 'multipart/form-data',
                },
                body: formData
            }
        )
        .then(res => res.json())
    );
}


export function addNewProductDetails(formData) {
    return(
        fetch(
            // 'http://172.20.10.5:8080/api/v1/products',
            `${baseUrl}/${endpoints.products}`,
            {
                method: 'POST',
                headers: {
                    'Accept': "application/json",
                    'Content-Type': 'multipart/form-data',
                },
                body: formData
            }
        )
        .then(res => res.json())
    );
}



// Customers
export function getAllCustomers() {
    return(
        fetch(
            `${baseUrl}/${endpoints.customers}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

// Customers Count
export function getCustomersCount() {
    return(
        fetch(
            `${baseUrl}/${endpoints.customers_count}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

export function createNewCustomer(body) {
    return(
        fetch(
            `${baseUrl}/${endpoints.customers}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}


export function updateCustomer(body, id) {
    return(
        fetch(
            `${baseUrl}/${endpoints.customers}/${id}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}


export function deleteCustomer(id) {
    return(
        fetch(
            `${baseUrl}/${endpoints.customers}/${id}`,
            {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}



// Employees

export function getAllEmployees() {
    return(
        fetch(
            `${baseUrl}/${endpoints.employees}`,
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

// Employees Count
export function getEmployeesCount() {
    return(
        fetch(
            `${baseUrl}/${endpoints.employees_count}`,
            {
                method: "GET",
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}


export function newEmployee(body) {
    return(
        fetch(
            `${baseUrl}/${endpoints.employees}`,
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}


export function updateEmployee(body, id) {
    return(
        fetch(
            `${baseUrl}/${endpoints.employees}/${id}`,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}


export function deleteEmployee(id) {
    return(
        fetch(
            `${baseUrl}/${endpoints.employees}/${id}`,
            {
                method: 'DELETE',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}













export function getAllUsers(query, token) {
    return(
        fetch(
            baseUrl + 'users',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}

export function createNewUser(body) {
    return(
        fetch(
            baseUrl + 'users',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: body
            }
        )
        .then(res => res.json())
    );
}






export function addOrderDetails(body) {
    return(
        fetch(
            orders_baseUrl + 'orders',
            {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}

export function getOrderDetails(body) {
    return(
        fetch(
            orders_baseUrl + 'orders',
            {
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                },
            }
        )
        .then(res => res.json())
    );
}


export function updateOrderDetails(body, id) {
    return(
        fetch(
            orders_baseUrl + 'orders/'+id,
            {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify(body) 
            }
        )
        .then(res => res.json())
    );
}


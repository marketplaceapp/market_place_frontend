import { CURRENT_USER_DETAILS, SET_AUTH_TOKEN } from "./actionTypes";

const INITIAL_STATE = {
    user: null,
    loader: false,
    user_logged_in: false,
    user_id: null,

    auth_token: null,

    current_user_details: null,
};

export default function auth(state = INITIAL_STATE, action) {
    switch (action.type) {
        
        case SET_AUTH_TOKEN: {
            return {
                ...state,
                auth_token: action.payload.auth_token,
            };
        }

        
        case CURRENT_USER_DETAILS: {
            const {
                id, name, email, profile_url, phone, isAdmin,
            } = action.payload
            
                return {
                    ...state,
                    current_user_details: {
                        id, name, email, profile_url, phone, isAdmin,
                    }
            };
        }

        
    case '@auth/USER_LOGGED_IN': {
        return {
            ...state,
            user_logged_in: action.payload.user_logged_in,
        }
    };
    
    case '@auth/USER_ID': {
    return {
        ...state,
            user_id: action.payload.user_id,
        }
    };


    case '@auth/SIGN_IN':
      return {
        ...state,
        user: action.payload.user,
      };
    case '@auth/SIGN_OUT':
      return { ...state, user: null };
    
    //   case '@auth/SCREEN_LOADER':
    //   return {
    //     ...state,
    //     loader: action.payload.loader,
    // };
    default:
      return state;
  }
}

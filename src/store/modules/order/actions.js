import { 
    ADD_ITEM_TO_CART,
    REMOVE_ITEM_FROM_CART,
    
    CURRENT_CUSTOMER_DETAILS,
    SET_CHARGES_DETAILS, 
    SET_CLOTH_IMAGES, 
    SET_DELIVERY_DETAILS, 
    SET_MEASUREMENT_DETAILS, 
    SET_OUTFIT_DETAILS,
    CLEAR_CART_LIST, 
} from './actionTypes';

export function addITemToCart(params) {
    return {
        type: ADD_ITEM_TO_CART,
        payload: {
            quantity: 1,
			id: params.id,
            name: params.name,
			description: params.description,
			price: params.price,
            image: params.image,
			discount_amount: params.discount_amount,
			category: params.category,
			sub_category: params.sub_category,
        },
    };
};

export const deleteItemFromCart = id => {
	return {
		  type: REMOVE_ITEM_FROM_CART,
		  payload: {
			id
		  },
	  };
};

export const clearCartList = () => {
	return {
		  type: CLEAR_CART_LIST,
		  payload: {},
	  };
};






export function setCurrentCustomer(params) {
    return {
        type: CURRENT_CUSTOMER_DETAILS,
        payload: params,
    };
};

export function setOutfitDetails(params) {
    return {
        type: SET_OUTFIT_DETAILS,
        payload: params,
    };
};

export function setClothImages(params) {
    return {
        type: SET_CLOTH_IMAGES,
        payload: params,
    };
};



export function setMeasurementDetails(params) {
    return {
        type: SET_MEASUREMENT_DETAILS,
        payload: params,
    };
};



export function setDeliveryDetails(params) {
    return {
        type: SET_DELIVERY_DETAILS,
        payload: params,
    };
};

export function setChargesDetails(params) {
    return {
        type: SET_CHARGES_DETAILS,
        payload: params,
    };
};
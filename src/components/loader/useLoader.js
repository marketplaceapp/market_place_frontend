import React, { useState } from 'react';

import { Text, View } from 'react-native';

const UseLoader = () => {
  const [loading, setLoading] = useState(false);

  return [
    loading ? 
    <View style={{flex: 1, position: 'absolute', alignItems: 'center', justifyContent: 'center' }}>
        <Text>Loading . . . </Text>
    </View>
    : null,
    () => setLoading(true),
    () => setLoading(false),
  ];
};

export default UseLoader;

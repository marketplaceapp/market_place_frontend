import React,{useEffect, useState} from 'react'
import { Text, View, StyleSheet, ScrollView, TouchableOpacity, Image, LogBox, Platform,  } from 'react-native'

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { getFocusedRouteNameFromRoute } from '@react-navigation/native';

import { colors } from '../../global/colors';
import { media } from '../../global/media';

import Icon from '../../utils/icons';

import { HomeStack, ChatStack, MyAdsStack, ProfileStack, SellStack } from '../dashboard';
import { MyAdsTopTabbar } from './MyAdsTopTabbar';
import { ChatsTopTabbar } from './ChatsTopTabbar';


const Tab = createBottomTabNavigator();


export default function Tabbar({navigation}) {

    LogBox.ignoreAllLogs(true)
 
    
    return (
        <Tab.Navigator
            screenOptions={({ route }) => ({
                tabBarIcon: ({ focused, color, size }) => {
                let iconName, routeName, height = 30, width = 30 ;
    
                if (route.name === 'HomeStack') {
                    iconName = focused ? media.home_fill : media.home
                    routeName = 'HOME' 
                } 
                else if (route.name === 'ChatStack') {
                    iconName = focused ? media.chat_fill : media.chat
                    routeName = 'CHAT'
                } else if (route.name === 'MyAdsStack') {
                    iconName = focused ? media.wishlist_fill : media.wishlist
                    routeName = 'MY ADS' 
                }
                else if (route.name === 'ProfileStack') {
                    iconName = focused ? media.admin_fill : media.admin
                    routeName = 'PROFILE'
                } 
                return(
                    <View style={{alignItems: 'center', justifyContent: 'center'}} >
                        <Image source={iconName} style={{height: height, width: width, resizeMode: 'contain'}}/>
                        <Text style={{marginTop: 4, fontSize: 14, color: focused ? 'black' : 'gray' }} >{routeName}</Text>
                    </View>
                );
                },
            })}
        >
            <Tab.Screen
                name="HomeStack"
                component={HomeStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName == 'ProductDetails' || routeName == 'Cart' || routeName == 'OrderConfirmation'   ||     routeName === 'AddCustomer' || routeName === 'ViewCustomer' || routeName === 'FabricScreen' || routeName === 'FabricDetails' || routeName === 'OrderConfirmed' ||  routeName === 'AddOrderDetails' || routeName === 'AddOrders' || routeName === 'AlterationScreen' || routeName === 'MeasurementScreen' || routeName === 'Notifications') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            <Tab.Screen
                name="ChatStack"
                component={ChatsTopTabbar}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitleAlign: 'center',
                    headerTitle: 'Inbox',
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName == 'CategoryStack' || routeName == 'CustomersStack' || routeName == 'EmployeesStack'   ||     routeName === 'AddCustomer' || routeName === 'ViewCustomer' || routeName === 'FabricScreen' || routeName === 'FabricDetails' || routeName === 'OrderConfirmed' ||  routeName === 'AddOrderDetails' || routeName === 'AddOrders' || routeName === 'AlterationScreen' || routeName === 'MeasurementScreen' || routeName === 'Notifications') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />


            <Tab.Screen
                name='SellStack'
                component={SellStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarButton: (props) => (
                        <TouchableOpacity
                            activeOpacity={0.85}
                            onPress={() => { navigation.navigate('SellStack', {screen: 'AddSell'}); }}
                            style={{top: -20, alignItems: 'center', justifyContent: 'center',}}
                        >
                            <View style={styles.addButtonContainer} >
                                <Image source={media.center_button} style={{height: 90, width: 90, }} />
                                
                            </View> 
                            <Text style={{fontSize: 14, color: 'gray', marginTop: 14 }} >SELL</Text>
                        </TouchableOpacity>
                    ),
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddSell' || routeName === 'SellSubCategory' || routeName === 'AdItemDetails') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                    })(route)
                })}
            />

            <Tab.Screen
                name="MyAdsStack"
                component={MyAdsTopTabbar}
                options={({navigation, route}) => ({
                    headerShown: true,
                    headerTitleAlign: 'center',
                    headerTitle: 'My Ads',
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName == 'CategoryStack' || routeName == 'CustomersStack' || routeName == 'EmployeesStack'   ||     routeName === 'AddCustomer' || routeName === 'ViewCustomer' || routeName === 'FabricScreen' || routeName === 'FabricDetails' || routeName === 'OrderConfirmed' ||  routeName === 'AddOrderDetails' || routeName === 'AddOrders' || routeName === 'AlterationScreen' || routeName === 'MeasurementScreen' || routeName === 'Notifications') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />
            <Tab.Screen
                name="ProfileStack"
                component={ProfileStack}
                options={({navigation, route}) => ({
                    headerShown: false,
                    tabBarShowLabel: false,
                    tabBarStyle: ((route) => {
                        const routeName = getFocusedRouteNameFromRoute(route) ?? ""
                        if (routeName === 'AddCustomer') {
                          return { display: "none",  }
                        }
                        return styles.tabbarStyle
                      })(route)
                })}
            />

        </Tab.Navigator>
    );
}



const styles = StyleSheet.create({
    tabbarStyle: {
        height: Platform.OS == 'ios' ? 90 : 70,
        paddingHorizontal: 12, 
        paddingTop: Platform.OS == 'ios' ? 15 : 0, 
        paddingBottom: Platform.OS == 'android' ? 2 : 30,
        borderTopWidth: 1, 
        // alignItems: 'center',
        // justifyContent: 'center',
        borderColor: colors.gray, 
        backgroundColor: colors.white
    },
    addButtonContainer: {
        height: 60,
        width: 60, 
        borderRadius: 30,
        borderWidth: 0.1,
        borderColor: colors.dark_gray,
        //backgroundColor: colors.primary,
        alignItems: 'center',
        justifyContent: 'center',
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 6,
        },
        shadowOpacity: 0.37,
        shadowRadius: 7.49,
        elevation: 10,
    }
})
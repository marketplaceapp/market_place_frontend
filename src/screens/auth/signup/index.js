import React, { useContext, useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, SafeAreaView, BackHandler, } from 'react-native'

import { media } from '../../../global/media'
import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'

import Input from '../../../components/input'
import PrimaryButton from '../../../components/button/PrimaryButton'
import { useDispatch } from 'react-redux'
import { userLoggedIn } from '../../../store/modules/auth/actions'
import UseLoader from '../../../components/loader/useLoader'
import { signupUser } from '../../../services/apiRequests'
import { ToastContext } from '../../../context/ToastContext'
import AppText from '../../../components/text'
import { STRING_CONSTANTS } from '../../../utils'
import AlreadyHaveAccount from '../../../components/AlreadyHaveAccount'

const SignupScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({});

    const [showEyeIcon, setShowEyeIcon] = useState(false);
 
    const [loader, showLoader, hideLoader] = UseLoader();
    const { showToast } = useContext(ToastContext);

    useEffect(() => {
        const backAction = () => {
            navigation.navigate('GetStarted')
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );
    
        return () => backHandler.remove();
    }, []);


    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }

    const signupFunction = () => {
        console.log("Dsad");
        //dispatch(userLoggedIn(true))
        //navigation.navigate('Tabbar')
        showLoader();
    
        if(checkForValidations()){
            console.log("Login details> > > ", form);
            var body = {
                "name": form.name,
                "email": form.email,
                "password": form.password,
                "number": form.number,
            }
            signupUserFunction(body)
        }else{
            hideLoader()
        }
    }


    const checkForValidations = () => {
        var isValid = true;

        if(!form.name){
            console.log("Please enter a valid name!");
            isValid = false
            setErrors((prev) => {
                return {...prev, name: 'Please enter a valid name!'}
            })
        }

        if(!form.password){
            console.log("Please enter a valid password!");
            isValid = false
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password!'}
            })
        }

        if(!form.email){
            console.log("Please enter a valid email address!");
            isValid = false
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email address!'}
            })
        }
        
        if(!form.number || form?.number?.length != 10){
            console.log("Please enter a valid mobile number!");
            isValid = false
            setErrors((prev) => {
                return {...prev, number: 'Please enter a valid mobile number!'}
            })
        }


        return isValid
    }


    const signupUserFunction = async (body) => {
        const onSuccess = (resp) => {
            console.log("resp > ",resp);
            hideLoader();
            showToast(resp?.message, true);

            navigation.navigate('LoginStack', {screen: 'Login'})
        };
        const onError = (err) => {
            console.log("err > ",err);
            hideLoader();
            showToast(err, false);
        };
        signupUser(
            body,
            onSuccess,
            onError
        );
    };

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView style={{width: '100%', padding: 20, flex: 1}} >
                
                <View>
                    <AppText
                        text="Create Account"
                        type={STRING_CONSTANTS.textConstants.HEADING}
                        customStyle={{textAlign: 'center'}}
                    />
                    <AppText
                        text="Fill your information below or register with your social account"
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{textAlign: 'center', paddingHorizontal: 40, marginVertical: 10}}
                    />
                </View>

                <Input
                    label="Name"
                    placeholder="Enter your name"
                    value={form.name}
                    error={errors.name}
                    onChangeText={(text) => {onChange({name: 'name', value: text,}); setErrors({}); }}
                />
                
                <Input
                    label="Email"
                    placeholder="Enter your email"
                    value={form.email}
                    error={errors.email}
                    onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                />

                <Input
                    label="Mobile Number"
                    placeholder="Enter your mobile number"
                    value={form.number}
                    isPhoneNumber
                    error={errors.number}
                    onChangeText={(text) => {onChange({name: 'number', value: text,}); setErrors({}); }}
                />

                <Input
                    label="Password"
                    placeholder="Enter your password"
                    isPassword
                    showEyeIcon={showEyeIcon}
                    error={errors.password}
                    onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                    value={form.password}
                    onChangeText={(text) => {onChange({name: 'password', value: text,}); setErrors({}); }}
                />

                <PrimaryButton 
                    title="Sign Up" 
                    buttonLoader={loader}
                    onPress={() => {
                        signupFunction()
                    }} 
                />
   
                <AlreadyHaveAccount
                    title="Already have an account?"
                    buttonText="Sign In"
                    onPress={() => {navigation.navigate('Login')}}
                />

            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.bg_color,
    },
})

export default SignupScreen
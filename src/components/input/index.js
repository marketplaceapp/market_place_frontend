import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'

import { Poppins } from '../../global/fontFamily'
import { media } from '../../global/media'
import { colors } from '../../global/colors'
import Icon from '../../utils/icons';


const Input = ({value, label, customStyle, customInputStyle, placeholder, isPhoneNumber, error, editable, keyboardType, onChangeEyeIcon, showEyeIcon, onChangeText, clearTextValue, isPassword, isSearch }) => {
    return (
        <View style={[styles.container, customStyle]} >
            {label && <Text style={styles.inputLabel} >{label}</Text>}

            <View style={[styles.inputContainer, editable == false && {backgroundColor: colors.light_gray}]} >
{/*             
                {isSearch && (
                    <View>
                        <Icon type="Feather" name="search" size={28} color={colors.black} style={{marginRight: 12}}  />
                    </View>
                )} */}

                <TextInput
                    value={value}
                    placeholder={placeholder}
                    placeholderTextColor={colors.gray }
                    onChangeText={onChangeText}
                    style={[styles.input, customInputStyle]}
                    autoCorrect={false}
                    editable={editable}
                    autoCapitalize={label == 'Email' ? 'none' : null}
                    maxLength={isPhoneNumber ? 10 : null}
                    keyboardType={keyboardType ? keyboardType : isPhoneNumber ? 'number-pad' : 'default'}
                    secureTextEntry={isPassword && !showEyeIcon ? true : false}
                />

                {/* {isPassword && (
                    <TouchableOpacity onPress={onChangeEyeIcon}>
                        <Image source={showEyeIcon ? media.view : media.hide} style={styles.iconImage} />
                    </TouchableOpacity>
                )}


                {isSearch && value?.length != 0 && (
                    <TouchableOpacity onPress={clearTextValue}>
                        <Icon type="AntDesign" name="close" size={28} color={colors.black} />
                    </TouchableOpacity>
                )} */}
            </View>

            {error && <Text style={styles.error} >{error ? error : null}</Text>}
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        //width: '100%',
        //flex: 1,
        marginBottom: 20,
    },
    input: {
        fontSize: 16,
        lineHeight: 22,
        height: 54,
        fontFamily: Poppins.Medium,
        flex: 1,
        marginRight: 10,
        color: colors.black,
    },
    inputContainer: {
        // borderRadius: 10,
        //flex: 1,
        // //width: '100%', 
        height: 54,       
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
        paddingHorizontal: 15,
        borderRadius: 8,
        borderWidth: 0.5,
        borderColor: colors.gray,
    },
    inputLabel: {
        fontSize: 14,
        marginBottom: 4,
        color: colors.black,
        fontFamily: Poppins.SemiBold,
    },
    iconImage: {
        height: 26,
        width: 26
    },
    error: {
        fontSize: 14,
        lineHeight: 18,
        marginTop: 6,
        fontFamily: Poppins.Medium,
        color: colors.red
    },

})

export default Input
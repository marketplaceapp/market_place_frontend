import { apiHeaders, pathUrls, requestParams } from './apiConstants';
import { getTokenFromLocalStorage } from './authServices';
import authFetch from './axios';


// Add My Product Ads
export const getMyProductAds = async ( 
    onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()
    return authFetch
        .get(pathUrls.get_owners_ads, 
            {headers: { Authorization: `${auth_token}` }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

// Product APIS

// Get Products
export const getProducts = async ( 
    onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()
    return authFetch
        .get(pathUrls.get_ads, 
            {headers: { Authorization: `${auth_token}` }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};


// Get Products By Id
export const getProductsById = async ( 
    id, onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()

    return authFetch
        .get(pathUrls.get_products_by_id + id, 
            {headers: { Authorization: `${auth_token}` }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

// Get Products By Category
export const getProductsByCategory = async ( 
    cat_id, onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()
    return authFetch
        .get(pathUrls.get_products_by_cat + cat_id, 
            {headers: { Authorization: `Bearer ${auth_token}` }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};


// Add Product
export const addProduct = async ( 
    body, onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()
    return authFetch
        .post(pathUrls.products, body,
            {headers: { 
                'Authorization': `${auth_token}`,
                'Accept': "application/json",
                'Content-Type': 'multipart/form-data',
            }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

// Update Product
export const updateProduct = async ( 
    body, onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()
    return authFetch
        .put(pathUrls.update_product, body, 
            {headers: { 
                'Authorization': `${auth_token}`,
                // 'Accept': "application/json",
                // 'Content-Type': 'multipart/form-data',
            }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

// Delete Product
export const deleteProduct = async( 
    id, onSuccess, onError, 
) => {
    const auth_token = await getTokenFromLocalStorage()
    return authFetch
        .delete(pathUrls.delete_product + id, 
            {headers: { Authorization: `${auth_token}` }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};


// Categories APIS

//get categories
export const getCategories = ( 
    token, onSuccess, onError, 
) => {
    return authFetch
        .get(pathUrls.categories, 
            {headers: { Authorization: `Bearer ${token}` }}
        )
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};



// Login APIS

// signup users
export const signupUser = ( 
    data, onSuccess, onError, 
) => {
    return authFetch
        .post(pathUrls.user_signup, data)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            onError(err);
        }
    );
};

// login users
export const loginUser = ( 
    data, onSuccess, onError, 
) => {
    return authFetch
        .post(pathUrls.user_login, data)
        .then((res) => {
            onSuccess(res?.data);
        },
        (err) => {
            console.log("ERROR > ", err);
            onError(err);
        }
    );
};

export const endpoints = {
    localhost_url: 'http://172.20.10.5:8080/api/v1',
    production_url: 'https://market-place-backend-5hoo.onrender.com/api/v1',

    base_url: 'https://market-place-backend-5hoo.onrender.com/api/v1',

    products: 'products',
    employees: 'employees',
    customers: 'customers',
    orders: 'orders',
    analytics: 'analytics',

    products_count: 'products/products_count',
    employees_count: 'employees/employees_count',
    customers_count: 'customers/customers_count',
    orders_count: 'orders/orders_count',

    analytics_count: 'analytics/analytics_count',
}
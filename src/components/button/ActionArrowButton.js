import React from 'react'
import { Text, View, ScrollView, SafeAreaView, StyleSheet, Image, TouchableOpacity } from 'react-native'
import { colors } from '../../global/colors'
import { screenWidth } from '../../global/constants'
import { fontSize } from '../../global/fontFamily'
import { media } from '../../global/media'
import Icon from '../../utils/icons'

const ActionArrowButton = ({ onPress, navigation, header}) => {
    return (
        <View style={{width: '100%', alignItems: 'flex-end', position: 'absolute', bottom: 20, right: 20}} >
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}
                style={[styles.container, {} ]}
            >
                <Icon type="AntDesign" name="arrowright" size={24} color={colors.white} />
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: colors.primary,
    },
})

export default ActionArrowButton

import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import EmptyData from '../../../components/empty'
import { media } from '../../../global/media'
 
const ChatScreen = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer}  >
                <EmptyData
                    image={media.no_chats}
                    title="You have got no messages so far!"
                    subTitle="Find something you like and start a conversation!"
                    buttonTitle="Start messaging"
                    onPress={() => {navigation.navigate('HomeStack', {screen: 'Home'})}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default ChatScreen
import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import Categories from '../../../components/custom/home/Categories'
import { categories } from '../../../global/sampleData'
import SubCategories from '../../../components/custom/home/SubCategories'
import { useNavigation } from '@react-navigation/native'
import { CustomHeader } from '../../../components/headers'
 
const SubCategoriesScreen = (props) => {
    const navigation = useNavigation()

    const [details, setDetails] = useState({});


    useEffect(() => {
        const params = props?.route?.params?.params
        setDetails(params)
    }, [])
    

    return (
        <SafeAreaView style={styles.container} >
             <CustomHeader
                title={details?.title}
                isBack={true}
            />
            <View style={styles.mainContainer}  >
               
                <SubCategories
                    data={details?.sub_category} 
                    horizontal={false}
                    onViewAll={() => {navigation.navigate('Categories')}} 
                    onCardPress={() => {navigation.navigate('Products')}} 
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        //flex: 1,
        padding: 20,
    }
})

export default SubCategoriesScreen
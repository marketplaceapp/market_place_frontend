import React, { useContext, useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, FlatList, SafeAreaView, BackHandler, } from 'react-native'

import { media } from '../../../global/media'
import { colors } from '../../../global/colors'
import { Poppins } from '../../../global/fontFamily'

import Input from '../../../components/input'
import PrimaryButton from '../../../components/button/PrimaryButton'
import { useDispatch } from 'react-redux'
import { setAuthToken, userLoggedIn } from '../../../store/modules/auth/actions'
import UseLoader from '../../../components/loader/useLoader'
import { ToastContext } from '../../../context/ToastContext'
import { loginUser } from '../../../services/apiRequests'
import { addUserToLocalStorage, getUserFromLocalStorage } from '../../../services/authServices'
import AppText from '../../../components/text'
import { STRING_CONSTANTS } from '../../../utils'
import ForgotPassword from '../../../components/ForgotPassword'
import AlreadyHaveAccount from '../../../components/AlreadyHaveAccount'


const LoginScreen = ({navigation}) => {

    const dispatch = useDispatch()

    const [form, setForm] = useState({})
    const [errors, setErrors] = useState({});

    const [showEyeIcon, setShowEyeIcon] = useState(false);
 
    const [loader, showLoader, hideLoader] = UseLoader();
    const { showToast } = useContext(ToastContext);


    useEffect(() => {
        const backAction = () => {
            navigation.goBack()
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );
    
        return () => backHandler.remove();
    }, []);



    const onChange = ({name, value}) => {
        setForm({...form, [name]: value})
        setErrors({})
    }

    const loginFunction = () => {
        showLoader();
    
        if(checkForValidations()){
            console.log("Login details > > > ", form);
            var body = {
                "email": form.email,
                "password": form.password,
            }
    
            console.log("body=> ", body);
            loginUserFunction(body)
        }else{
            hideLoader()
        }
    }


    const checkForValidations = () => {
        var isValid = true;

        if(!form.password){
            console.log("Please enter a valid password!");
            isValid = false
            setErrors((prev) => {
                return {...prev, password: 'Please enter a valid password!'}
            })
        }

        if(!form.email){
            console.log("Please enter a valid email address!");
            isValid = false
            setErrors((prev) => {
                return {...prev, email: 'Please enter a valid email address!'}
            })
        }

        return isValid
    }


    const loginUserFunction = async (body) => {
        console.log("body> ",body);
        const onSuccess = (resp) => {
            console.log("login resp > ",resp);
            hideLoader();
            showToast(resp?.message, true);

            dispatch(userLoggedIn(true))
            dispatch(setAuthToken(resp?.token))

            addUserToLocalStorage(resp?.data, resp?.token)

            navigation.navigate('Tabbar')
        };
        const onError = (err) => {
            console.log("login err > ",err?.response?.data?.message);
            hideLoader();
            showToast(err?.response?.data?.message, false);
        };
        loginUser(
            body,
            onSuccess,
            onError
        );
    };

    return (
        <SafeAreaView style={styles.container} >
            <ScrollView style={{width: '100%', padding: 20, flex: 1}} >
                
                <View>
                    <AppText
                        text="Sign In"
                        type={STRING_CONSTANTS.textConstants.HEADING}
                        customStyle={{textAlign: 'center'}}
                    />
                    <AppText
                        text="Hi! Welcome back, you've been missed"
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{textAlign: 'center', paddingHorizontal: 40, marginVertical: 10}}
                    />
                </View>

                <Input
                    label="Email"
                    placeholder="Enter your email"
                    value={form.email}
                    error={errors.email}
                    onChangeText={(text) => {onChange({name: 'email', value: text,}); setErrors({}); }}
                />

                <Input
                    label="Password"
                    placeholder="Enter your password"
                    isPassword
                    showEyeIcon={showEyeIcon}
                    error={errors.password}
                    onChangeEyeIcon={() => setShowEyeIcon(!showEyeIcon)}
                    value={form.password}
                    onChangeText={(text) => {onChange({name: 'password', value: text,}); setErrors({}); }}
                />

                <ForgotPassword
                    onPress={() => {navigation.navigate('ForgotPassword')}}
                />

                <PrimaryButton 
                    title="Login" 
                    buttonLoader={loader}
                    buttonStyle={{marginTop: 30}}
                    onPress={() => {
                        loginFunction()
                    }} 
                />
                
                <AlreadyHaveAccount
                    title="Don't have an account?"
                    buttonText="Sign Up"
                    onPress={() => {navigation.navigate('Signup')}}
                />

            </ScrollView>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'space-between',
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        width: '100%',
        height: '100%',
        padding: 20,
        minHeight: 500,
        borderTopLeftRadius: 20,
        borderTopRightRadius: 20,
        backgroundColor: colors.bg_color,
    },
    header: {
        width: '100%',
        padding: 15,
        paddingVertical: 100,
        alignItems: 'center'
    },
    headerTitle: {
        color: colors.white,
        fontSize: 18,
        lineHeight: 22,
        fontFamily: Poppins.Medium,
    },
    title: {
        color: colors.black,
        fontSize: 18,
        lineHeight: 22,
        marginBottom: 10,
        textAlign: 'center',
        fontFamily: Poppins.Medium,
    }
})

export default LoginScreen
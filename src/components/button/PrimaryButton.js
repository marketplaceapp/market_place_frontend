import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { screenWidth } from '../../global/constants'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import Loader from '../loader'
 

const PrimaryButton = ({title, disabled, buttonLoader, buttonStyle, loaderColor, onPress}) => {

    return (
        <View style={[styles.container, buttonStyle]} >
            {title != 'Delete'
            ?
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.button, {backgroundColor: disabled ? colors.gray : colors.primary}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader loaderColor={loaderColor} /> : <Text style={styles.title} >{title}</Text>}
            </TouchableOpacity>
            :
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.deleteButton, {backgroundColor: disabled ? colors.gray : colors.white}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader loaderColor={colors.red} /> : <Text style={styles.deleteTitle} >{title}</Text>}
            </TouchableOpacity>
            }
            
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 14,
    },
    button: {
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
    },
    deleteButton: {
        height: 54,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 8,
        borderWidth: 1,
        borderColor: colors.red,
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    },
    deleteTitle: {
        fontSize: 18,
        fontFamily: Poppins.Medium,
        color: colors.red,
    }
})

export default PrimaryButton
import React from 'react'
import { Text, View, StyleSheet } from 'react-native'
import { BackButton } from '../button'
import AppText from '../text'
import { STRING_CONSTANTS, colors } from '../../utils'

const CustomHeader = ({title, onPress, isBack}) => {
    return (
        <View style={[styles.container, isBack ? styles.spaceBetween : styles.center]} >
            {isBack && (<View>
                <BackButton  />
            </View>)}
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.HEADLINE2}
                customStyle={{textAlign: 'center', color: colors.primary}}
            />
            <View style={{width: 40}} >
                
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        height: 60,
        width: '100%',
        paddingHorizontal: 15,
        paddingVertical: 10,
    },
    spaceBetween: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    center: {
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default CustomHeader
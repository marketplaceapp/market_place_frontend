import React from 'react'
import { Text, View, Image, StyleSheet, FlatList, TouchableOpacity, BackHandler, SafeAreaView  } from 'react-native'
import { wishlist_categories } from '../../../global/sampleData'
import AppText from '../../text'
import { STRING_CONSTANTS, colors } from '../../../utils'


const WishlistCategory = ({ currentCategory, onChangeCategory }) => {

    const Category = ({key, title, }) => {
        return(
            <TouchableOpacity 
                key={title} 
                style={[styles.categoryContainer, {backgroundColor: currentCategory == title ? colors.primary : colors.white}]} 
                onPress={() => {onChangeCategory(title)}}
                >
                <AppText
                    text={title}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{textAlign: 'center', color: currentCategory == title ? colors.white : colors.black }}
                />
            </TouchableOpacity>
        )
    }

    return (
        <View style={styles.container} >
            <FlatList
                data={wishlist_categories}
                keyExtractor={item => item?.title}
                horizontal
                renderItem={({item}) => <Category title={item?.title} key={item?.title} />}
            />
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        marginVertical: 12,
    },
    categoryContainer: {
        height: 40,
        marginRight: 12,
        paddingHorizontal: 25,
        borderRadius: 20,
        borderWidth: 0.5,
        borderColor: colors.gray,
        alignItems: 'center',
        justifyContent: 'center',
    }
})

export default WishlistCategory
import React from 'react'
import { Text, View } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';
import MyAdsScreen from '../../screens/dashboard/myAds';
import Favourites from '../../screens/dashboard/myAds/Favourites';
import { colors } from '../../utils';

const Tab = createMaterialTopTabNavigator();

export const MyAdsTopTabbar = () => {

    const screenOptions = {
        tabBarActiveTintColor: colors.primary,
        tabBarInactiveTintColor: 'gray',
        tabBarLabelStyle: { fontSize: 16, fontWeight: 'bold' },
        tabBarStyle: { 
            backgroundColor: '#fdfdfd',
        },
        tabBarIndicatorStyle: {
            backgroundColor: colors.primary
        },
    
    };


    return (
        <Tab.Navigator
            screenOptions={screenOptions}
            
        >
            <Tab.Screen 
                name="MyAdsScreen" 
                component={MyAdsScreen} 
                 options={{
                    title: 'Ads'
                }}
            />
            <Tab.Screen 
                name="Favourites" 
                component={Favourites} 
                 options={{
                    title: 'Favourites'
                }}
            />
        </Tab.Navigator>
    );
}
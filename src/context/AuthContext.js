import React, { createContext, useEffect, useState } from 'react';
import {Text, View, Image, StyleSheet, } from 'react-native'

import { colors } from '../global/colors';
import { Poppins } from '../global/fontFamily';

import { getTokenFromLocalStorage, getUserFromLocalStorage } from '../services/authServices';

export const AuthContext = createContext();

export const AuthProvider = ({ children }) => {

    const [authToken, setAuthToken] = useState(null);

    useEffect(() => {
        getToken()
    }, [])
    

    const getToken = async () => {
        const token = await getTokenFromLocalStorage()
        setAuthToken(token)
    }

    return (
        <AuthContext.Provider 
            value={{ 
                authToken 
            }}
        >
            {children}
        </AuthContext.Provider>
    );
};



const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 30,
        width: '100%',
    },
    mainContainer: {
        flex: 1,
        height: 55,
        borderRadius: 10,
        marginHorizontal: 20,
        paddingHorizontal: 15,
        borderWidth: 1,
        backgroundColor: colors.black,
        borderColor: colors.primary,
        alignItems: 'center',
        flexDirection: 'row',
    },
    title: {
        fontSize: 14,
        color: colors.white,
        fontFamily: Poppins.Medium,
        marginLeft: 14,
    },
    shadowStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 4,
    },
})


export const colors = {
    //primary: '#0C2609',
    primary: '#012f34',
    bg_color: '#fbfbfb',
    black: '#282828',
    white: '#fff',
    green: '#007c02',
    dark_gray: '#3d3d3d',


    brown: '#EA9F5F',
    red: '#FF0000',

    cream: '#E8B88D',
    light_cream: '#F0E3D5',

    light_red: '#FEA7A9',

    dark_blue: '#212c46',
    

    
    gray: '#919399',
    light_gray: '#ebecf0',
    
    blue: '#81b3f3',

    orange: '#f7c191',
    reddish: '#ED4545',
}
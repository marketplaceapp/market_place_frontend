
import { 
    SET_SHOW_TOAST_MESSAGE,
} from "./actionTypes";

const INITIAL_STATE = {
    toast_message: null,
};

export default function home(state = INITIAL_STATE, action) {
    switch (action.type) {

        case SET_SHOW_TOAST_MESSAGE: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                toast_message: action.payload
            }
        };

        default:
        return state;
    }
}

import { 
    SET_SHOW_TOAST_MESSAGE,
} from './actionTypes';


export function showToastMessage(params) {
    return {
        type: SET_SHOW_TOAST_MESSAGE,
        payload: params
    };
};

import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, BackHandler, SafeAreaView  } from 'react-native'
import AppText from './text'
import { STRING_CONSTANTS, colors } from '../utils'


const AlreadyHaveAccount = ({title, buttonText, onPress}) => {
    return (
        <View style={styles.container} >
            <AppText
                text={title}
                type={STRING_CONSTANTS.textConstants.BODY}
                customStyle={{textAlign: 'center'}}
            />
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}    
            >
                <AppText
                    text={buttonText}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: colors.primary, textDecorationLine: 'underline', marginLeft: 4}}
                />
            </TouchableOpacity>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default AlreadyHaveAccount
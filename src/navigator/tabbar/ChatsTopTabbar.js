import React from 'react'
import { Text, View } from 'react-native'
import { createMaterialTopTabNavigator } from '@react-navigation/material-top-tabs';

import { colors } from '../../utils';

import ChatScreen from '../../screens/dashboard/chat';
import BuyingChatScreen from '../../screens/dashboard/chat/BuyingChat';
import SellingChatScreen from '../../screens/dashboard/chat/SellingChat';

const Tab = createMaterialTopTabNavigator();

export const ChatsTopTabbar = () => {

    const screenOptions = {
        tabBarActiveTintColor: colors.primary,
        tabBarInactiveTintColor: 'gray',
        tabBarLabelStyle: { fontSize: 16, fontWeight: 'bold' },
        tabBarStyle: { 
            backgroundColor: '#fdfdfd',
        },
        tabBarIndicatorStyle: {
            backgroundColor: colors.primary
        }
    };


    return (
        <Tab.Navigator
            screenOptions={screenOptions}
        >
            <Tab.Screen 
                name="Chat" 
                component={ChatScreen}
                options={{
                    title: 'All'
                }}
            />
            <Tab.Screen 
                name="BuyingChat" 
                component={BuyingChatScreen} 
                options={{
                    title: 'Buying'
                }}
            />
            <Tab.Screen 
                name="SellingChat" 
                component={SellingChatScreen} 
                options={{
                    title: 'Selling'
                }}
            />
        </Tab.Navigator>
    );
}
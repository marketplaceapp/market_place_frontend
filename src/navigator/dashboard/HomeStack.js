import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import HomeScreen from '../../screens/dashboard/home';
import CategoriesScreen from '../../screens/dashboard/categories';
import ProductScreen from '../../screens/dashboard/home/ProductScreen';
import SubCategoriesScreen from '../../screens/dashboard/categories/SubCategories';
import ProductDetailsScreen from '../../screens/dashboard/home/ProductDetails';
import CartScreen from '../../screens/dashboard/cart';
import OrderConfirmation from '../../screens/dashboard/cart/OrderConfirmation';

const Stack = createStackNavigator();

const HomeStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Home" 
        >
            <Stack.Screen
                name="Home"
                component={HomeScreen}
                options={{
                    headerShown: false,
                    // headerTitle: 'Home',
                    // headerLeft: () => null,
                    // headerTitleAlign: 'center',
                    // headerStyle: {backgroundColor: colors.black},
                    // headerTitleStyle: {color: colors.white, fontSize: fontSize.Title, fontFamily: Poppins.SemiBold},
                }}
            />
            <Stack.Screen
                name="Products"
                component={ProductScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ProductDetails"
                component={ProductDetailsScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="Categories"
                component={CategoriesScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="SubCategories"
                component={SubCategoriesScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="OrderConfirmation"
                component={OrderConfirmation}
                options={{
                    headerShown: false,
                }}
            />


        </Stack.Navigator>
    );
}

export default HomeStack
import React, { useState, useEffect } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import Categories from '../../../components/custom/home/Categories'
import { categories } from '../../../global/sampleData'
import { CustomHeader } from '../../../components/headers'
 
const CategoriesScreen = ({navigation}) => {

    useEffect(() => {
        console.log('CategoriesScreen >>> ');
   
    }, [])
    

    const viewSubCategory = (val) => {
        console.log("val> > ",val);
        //navigation.navigate('SubCategories')

        if(val?.sub_category){
            navigation.navigate('SubCategories', {params: val})
        }else{
            navigation.navigate('Products', {params: val})
        }
    }

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader
                title="Categories"
                isBack={true}
            />
            <View style={styles.mainContainer}  >

                <Categories
                    data={categories} 
                    horizontal={false}
                    onViewSubCat={(val) => viewSubCategory(val)}
                    onViewAll={() => {navigation.navigate('Categories')}} 
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        //flex: 1,
        padding: 20,
    }
})

export default CategoriesScreen
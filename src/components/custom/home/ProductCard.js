import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView, FlatList } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { screenWidth } from '../../../global/constants'
import AppText from '../../text'
import { media } from '../../../global/media'
 
const ProductCard = ({ data, title, onViewAll, viewProductDetails  }) => {
    return (
        <View style={styles.container} >
            {title && (
            <View style={{marginBottom: 10, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between'}} >
                <AppText
                    text={title}
                    type={STRING_CONSTANTS.textConstants.HEADLINE1}
                    customStyle={{marginTop: 4, }}
                    numberOfLines={1}
                />
                {onViewAll && (
                    <TouchableOpacity 
                        onPress={onViewAll}
                        activeOpacity={0.5}
                    >
                        <AppText
                            text={'View all'}
                            type={STRING_CONSTANTS.textConstants.BODY}
                            customStyle={{marginTop: 4, color: colors.gray, textDecorationLine: 'underline'}}
                            numberOfLines={1}
                        />
                    </TouchableOpacity>)}
            </View>)}

            <FlatList
                data={data}
                showsVerticalScrollIndicator={false}
                keyExtractor={item => item?.name}
                contentContainerStyle={{flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-between',}}
                renderItem={({item, index}) => {

                    const { id, name, price, description, image, status } = item

                    return(
                        <TouchableOpacity 
                            activeOpacity={0.5}
                            onPress={() => viewProductDetails(item)}
                            key={id} style={styles.cardContainer} >
                            <View>
                                <Image source={{uri: image}} style={styles.cardImg} />
                                <TouchableOpacity
                                    activeOpacity={0.5}
                                    onPress={() => {}}
                                    style={{position: 'absolute', right: 10, top: 10, height: 40, width: 40, borderRadius: 20, backgroundColor: '#0F172A20', alignItems: 'center', justifyContent: 'center'}}
                                >
                                    <Image source={true ? media.heart_white : media.heart_white_fill} style={true ? {height: 30, width: 30, resizeMode: 'contain'} : {height: 24, width: 24, resizeMode: 'contain'}} />
                                </TouchableOpacity>

                                {status == 'sold' && (
                                    <View style={{position: 'absolute', bottom: 0,  width: '100%', alignItems: 'center', justifyContent: 'center', borderRadius: 4, backgroundColor: colors.reddish}} >
                                        <AppText
                                            text={'Sold'}
                                            type={STRING_CONSTANTS.textConstants.BODY}
                                            customStyle={{marginTop: 2, marginRight: 4, color: colors.white}}
                                            numberOfLines={1}
                                        />
                                    </View>
                                )}
                            

                            </View>
                            <View style={styles.cardContent} >

                            
                                <AppText
                                    text={`Rs. ${price}`}
                                    type={STRING_CONSTANTS.textConstants.HEADLINE2}
                                    customStyle={{}}
                                    numberOfLines={1}
                                />
                                <AppText
                                    text={name}
                                    type={STRING_CONSTANTS.textConstants.BODY}
                                    customStyle={{}}
                                    numberOfLines={1}
                                />
                               <View style={{height: 40}} >
                               <AppText
                                    text={description}
                                    type={STRING_CONSTANTS.textConstants.BODY}
                                    customStyle={{color: colors.gray}}
                                    numberOfLines={2}
                                />
                               </View>
                            </View>
                        </TouchableOpacity>
                    )
                }}
            />
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
    },
    cardContainer: {
        borderWidth: 1,
        marginBottom: 12,
        borderRadius: 8,
        width: screenWidth/2-25,
    },
    cardContent: {
       padding: 10,
    },
    cardImg: {
        height: 150, 
        width: '100%',
        //resizeMode: 'contain',
        borderTopLeftRadius: 8,
        borderTopRightRadius: 8,
        backgroundColor: colors.light_gray,
    }
})

export default ProductCard
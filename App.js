import React, { useEffect } from 'react';
import { Text, View, LogBox, StatusBar, SafeAreaView, Image } from 'react-native';

import { Provider } from 'react-redux';
import { PersistGate } from 'redux-persist/integration/react';

import { store, persistor } from './src/store';
import { ToastProvider } from './src/context/ToastContext';
import { AuthProvider } from './src/context/AuthContext';

import { Navigator } from './src/navigator';
import { NavigationContainer } from '@react-navigation/native';

import SplashScreen from 'react-native-splash-screen';

LogBox.ignoreAllLogs(true);

const App = () => {

    useEffect(() => {
        setTimeout(() => {
            SplashScreen.hide()
        }, 1000);
    }, [])
    
    return (
        <Provider store={store}>
            <PersistGate persistor={persistor}>
                <AuthProvider>
                    <ToastProvider>
                        <NavigationContainer>
                            <Navigator />
                        </NavigationContainer>
                    </ToastProvider>
                </AuthProvider>
            </PersistGate>
        </Provider>
    )
}

export default App

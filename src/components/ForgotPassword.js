import React from 'react'
import { Text, View, Image, StyleSheet, TouchableOpacity, BackHandler, SafeAreaView  } from 'react-native'
import AppText from './text'
import { STRING_CONSTANTS, colors } from '../utils'


const ForgotPassword = ({ onPress}) => {
    return (
        <View style={styles.container} >
            
            <TouchableOpacity
                activeOpacity={0.5}
                onPress={onPress}    
            >
                <AppText
                    text={"Forgot Password"}
                    type={STRING_CONSTANTS.textConstants.BODY}
                    customStyle={{color: colors.primary, textDecorationLine: 'underline', }}
                />
            </TouchableOpacity>
            
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        alignItems: 'flex-end'
    }
})

export default ForgotPassword
import React, {useState, useEffect} from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import AsyncStorage from '@react-native-async-storage/async-storage';

import OnboardingScreen from '../../screens/onboarding';
import GetStarted from '../../screens/onboarding/GetStarted';

import { ForgotPassword, LoginScreen, SignupScreen, VerifyOtp } from '../../screens/auth';


const Stack = createStackNavigator();

const LoginStack = ({navigation}) => {

    const [onboardingDetails, setOnboardingDetails] = useState(null);

    useEffect(() => {
        getOnboardingDetails()
    }, [])
    
    const getOnboardingDetails = async () => {
      
        let details = await AsyncStorage.getItem('onboarding_done');  
        let onboarding_done = JSON.parse(details);  
      
        setOnboardingDetails(onboarding_done)
    }

    return (
        <Stack.Navigator 
            initialRouteName={onboardingDetails ? "Login" : "GetStarted"}
        >
            
            <Stack.Screen
                name="GetStarted"
                component={GetStarted}
                options={{
                    headerShown: false,
                }}
            />
            
            <Stack.Screen
                name="Onboarding"
                component={OnboardingScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="Login"
                component={LoginScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="Signup"
                component={SignupScreen}
                options={{
                    headerShown: false,
                }}
            />

            <Stack.Screen
                name="VerifyOtp"
                component={VerifyOtp}
                options={{
                    headerShown: false,
                }}
            />
            <Stack.Screen
                name="ForgotPassword"
                component={ForgotPassword}
                options={{
                    headerShown: false,
                }}
            />

        </Stack.Navigator>
    );
}

export default LoginStack
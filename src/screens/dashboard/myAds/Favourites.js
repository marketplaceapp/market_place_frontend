import React from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { colors } from '../../../utils'
import EmptyData from '../../../components/empty'
import { media } from '../../../global/media'
 
const Favourites = ({navigation}) => {
    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.mainContainer}  >
                <EmptyData
                    image={media.no_liked}
                    title="You haven't liked anything yet"
                    subTitle="Mark the items that you like and share it with the world!"
                    buttonTitle="Discover"
                    onPress={() => {navigation.navigate('HomeStack', {screen: 'Home'})}}
                />
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
        padding: 20,
        alignItems: 'center',
        justifyContent: 'center'
    }
})

export default Favourites
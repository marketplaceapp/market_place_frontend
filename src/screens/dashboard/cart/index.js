import React, { useState, useEffect, useContext } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, FlatList, SafeAreaView, TextInput } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import { useSelector } from 'react-redux';
import { CustomHeader } from '../../../components/headers';
import ProductList from '../../../components/custom/home/ProductList';
import EmptyData from '../../../components/empty';
import { media } from '../../../global/media';
import { PrimaryButton } from '../../../components/button';
import AppText from '../../../components/text';
import UseLoader from '../../../components/loader/useLoader';
import { ToastContext } from '../../../context/ToastContext';
import { updateProduct } from '../../../services/apiRequests';
 
const CartScreen = ({navigation}) => {

    const cart_list = useSelector(state => state.order.cart_list);

    const [totalPrice, setTotalPrice] = useState(0);
    const [subTotalPrice, setSubTotalPrice] = useState(0);
    const [delivieryPrice, setDelivieryPrice] = useState(140);
    const [discountPrice, setDiscountPrice] = useState(300);

    const [loader, showLoader, hideLoader] = UseLoader();
    const { showToast } = useContext(ToastContext);


    useEffect(() => {
        var price = cart_list.reduce((n, { price }) => n + price, 0);
        
        var deliveryFee = 140

        var total_price = price + deliveryFee
        
        setTotalPrice(total_price);
        setSubTotalPrice(price);

        setDiscountPrice(total_price > 1000 ? 300 : 100)
    }, [cart_list]);


    const confirmOrder = async () => {

        const arrayOfIds = cart_list.map(obj => obj.id);
        console.log(">>arrayOfIds > ",arrayOfIds);

        showLoader()
        var body = {
            "productIds": arrayOfIds
        }
      
        const onSuccess = (resp) => {
            console.log("update product resp > ",resp);
            hideLoader();
            showToast(resp?.message, true);

            navigation.navigate('OrderConfirmation')
        };
        const onError = (err) => {
            console.log("update product err > ",err);
            hideLoader();
            showToast(err, false);
        };
        updateProduct(
            body,
            onSuccess,
            onError
        );
    };

    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader
                title={'My Cart'}
                isBack={true}
            />
            {cart_list?.length > 0 ? (
                <View style={styles.mainContainer}  >
                    <View style={{paddingHorizontal: 20, flex: 1,}} >
                        <ProductList
                            data={cart_list}
                            viewProductDetails={(val) => {}}
                        />

                    </View>

                    <View style={styles.checkoutContainer} >

                        <View style={styles.promoContainer} >
                            <View>
                                <TextInput
                                    placeholder='Promo Code'
                                    style={{paddingLeft: 12}}
                                />
                            </View>
                            <TouchableOpacity
                                onPress={() => {}}
                                activeOpacity={0.5}
                                style={styles.applyContainer}
                            >
                                <AppText
                                    text={'Apply'}
                                    type={STRING_CONSTANTS.textConstants.BODY}
                                    customStyle={{color: colors.white}}
                                    numberOfLines={1}
                                />
                            </TouchableOpacity>
                        </View>

                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}} >
                            <AppText
                                text={'Sub-Total'}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{}}
                                numberOfLines={1}
                            />
                            <AppText
                                text={subTotalPrice}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{color: colors.gray}}
                                numberOfLines={2}
                            />
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}} >
                            <AppText
                                text={'Delivery Fee'}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{}}
                                numberOfLines={1}
                            />
                            <AppText
                                text={delivieryPrice}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{color: colors.gray}}
                                numberOfLines={2}
                            />
                        </View>
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}} >
                            <AppText
                                text={'Discount'}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{}}
                                numberOfLines={1}
                            />
                            <AppText
                                text={discountPrice}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{color: colors.gray}}
                                numberOfLines={2}
                            />
                        </View>
                        <View style={{borderBottomWidth: 1, borderStyle: 'dashed', height: 10, marginBottom: 10}} />
                        <View style={{flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between',}} >
                            <AppText
                                text={'Total Cost'}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{}}
                                numberOfLines={1}
                            />
                            <AppText
                                text={totalPrice}
                                type={STRING_CONSTANTS.textConstants.BODY}
                                customStyle={{color: colors.gray}}
                                numberOfLines={2}
                            />
                        </View>
                        
                        <PrimaryButton
                            title='Proceed to Checkout'
                            buttonLoader={loader}
                            onPress={() => {confirmOrder()}}
                        />
                    </View>
                </View>)
                :
                <View style={{flex: 1, padding: 20, alignItems: 'center', justifyContent: 'center'}} >
                    <EmptyData
                        image={media.no_chats}
                        title="You have got no items so far!"
                        subTitle="Find something you like and start add to cart!"
                        buttonTitle="Start shopping"
                        onPress={() => {navigation.navigate('HomeStack', {screen: 'Home'})}}
                    />
                </View>
            }
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        flex: 1,
       
        justifyContent: 'space-between'
    },
    checkoutContainer: {
        padding: 20,
        paddingBottom: 0,
        borderTopLeftRadius: 12,
        borderTopRightRadius: 12,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 0.27,
        elevation: 2,
    },
    promoContainer: {
        borderWidth: 1, 
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
        borderRadius: 8,
        marginBottom: 10,
        padding: 4,
        height: 50,
    },
    applyContainer: {
        paddingHorizontal: 20,
        borderRadius: 8,
        height: 40,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: colors.primary
    }
})

export default CartScreen
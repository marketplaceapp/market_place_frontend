import { Linking } from "react-native";
import { media } from "../global/media";

import AsyncStorage from "@react-native-async-storage/async-storage";
import Clipboard from '@react-native-clipboard/clipboard';


export const getNameInitials = (string) => {
    var names = string.split(' '),
        initials = names[0].substring(0, 1).toUpperCase();
    
    if (names.length > 1) {
        initials += names[names.length - 1].substring(0, 1).toUpperCase();
    }
    return initials;
};


export const getStepBarWidth = (totalSteps, currentStep) => {
    var step = 100/totalSteps
    var finalWidth = `${step*currentStep}%`

    //console.log("finalWidth= > > ",finalWidth, step, currentStep);
    switch(currentStep){
        case 1: 
        return finalWidth;

        case 2: 
        return finalWidth;

        case 3: 
        return finalWidth;

        case 4: 
        return finalWidth;

        case 5: 
        return finalWidth;
    
        default: 
        return '100%';
    }
}

export const isEmpty = (obj) => {
    for (const prop in obj) {
      if (Object.hasOwn(obj, prop)) {
        return false;
      }
    }
  
    return true;
}


export const getCategoryImage = (value) => {
    var img;
    switch (value) {
        case 'Shirts': 
            img = media.shirt
            break;
        case 'Pants': 
            img = media.pant
            break;
        case 'Blazer': 
            img = media.blazer
            break;
        case 'Suit': 
            img = media.suit
            break;
        case 'Kurta': 
            img = media.kurta
            break;
        case 'Waistcoat': 
            img = media.waistcoat
            break;
        case 'Ladies Coat': 
            img = media.ladies
            break;
        default:
            img = media.shirt
            break;
    }

    return img;
}   

export const openCallLog = (number) => {
    Linking.openURL(`tel:${number}`);
}

export const openWhatsappMessage = (number) => {
    let url = 'whatsapp://send?text=' + '' + '&phone=91' + number;
    Linking.openURL(url)
    .then((data) => {
        console.log('WhatsApp Opened');
    })
    .catch(() => {
        console.log("Error");
    });
}

export const openGoogleMaps = (placeName) => {
    const encodedPlaceName = encodeURIComponent(placeName);
    const mapsUrl = `https://www.google.com/maps/search/?api=1&query=${encodedPlaceName}`;
  
    Linking.openURL(mapsUrl);
};

export const opneInvoiceFile = (invoiceLink) => {
    Linking.openURL(invoiceLink)
    .then((data) => {
        console.log('Invoice Opened');
    })
    .catch(() => {
        console.log("Error");
    });
};

export const shareInvoiceLink = (number, invoiceLink) => {
    let url = 'whatsapp://send?text=' + invoiceLink + '&phone=91' + number;
    Linking.openURL(url)
    .then((data) => {
        console.log('WhatsApp Opened');
    })
    .catch(() => {
        console.log("Error");
    });
};


export const copyText = (text) => {
    Clipboard.setString(text);
    console.log('Text Copied > ', text);
};



  

export const getThemeFromLocalStorage = async () => {

    let theme = await AsyncStorage.getItem('theme');  

    if (!theme) return null;
    return theme;
};

export const addThemeToLocalStorage = async (theme) => {
    AsyncStorage.setItem('theme', theme);
};


export const getLanguageFromLocalStorage = async () => {

    let language = await AsyncStorage.getItem('language');  

    if (!language) return null;
    return language;
};

export const addLanguageToLocalStorage = async (language) => {
    AsyncStorage.setItem('language', language);
};



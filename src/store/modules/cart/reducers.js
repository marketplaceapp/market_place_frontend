
import { 
    ADD_PRODUCTS_TO_CART, CLEAR_PRODUCTS_FROM_CART,
} from "./actionTypes";


const INITIAL_STATE = {
    cart_list: [],
};

export default function cart(state = INITIAL_STATE, action) {
    switch (action.type) {

        case ADD_PRODUCTS_TO_CART: {
            const {
                id, name, number, address, city, addressState, pincode,
            } = action.payload
            return {
                ...state,
                cart_list: [...state.cart_list, action.payload]
            }

            //  const updatedCartList = [...state.cart_list, action.payload];

            //  return {
            //      ...state,
            //      cart_list: updatedCartList,
            //  };

        };


        case CLEAR_PRODUCTS_FROM_CART: {
            const { } = action.payload

            return {
                ...state,
                cart_list: []
            }


        };

        default:
        return state;
    }
}

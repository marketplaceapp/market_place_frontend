import AsyncStorage from "@react-native-async-storage/async-storage";
import moment from "moment";

export const orderId = () => {

    const S4 = () => {
      return (((1 + Math.random()) * 0x10000) | 0).toString(16).substring(1);
    };
  
    return (
      S4() +  S4() + "-" + S4() + "-" + S4() + "-" + S4() + "-" + S4() + S4() + S4()
    );
}

export const getStatusValue = (val) => {
    var status = ''
    if(val == 'pending')
        status = 'Pending'
    else if(val == 'in_progress')
        status = 'In Progress'
    else if(val == 'completed')
        status = 'Completed'

    return status
}   

export const productId = () => {
  return Math.floor(100000 + Math.random()*(999999 - 100000 + 1))
}   

export const capitalizeEachWord = (str) => {
    return str.split(' ').map(word => word.charAt(0).toUpperCase() + word.slice(1)).join(' ');
}

export const formatDate = (date, format) => {
  return moment(date).local().format(format || 'ddd, DD/MM/YY | HH:mm A')
}   

export const formatDateOnly = (date) => {
  return moment(date).local().format('dddd, DD-MM-YYYY')
}   


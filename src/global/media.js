export const media = {
    home_fill: require('../assets/icons/tabbar/home_fill.png'),
    admin_fill: require('../assets/icons/tabbar/admin_fill.png'),
    wishlist_fill: require('../assets/icons/tabbar/wishlist_fill.png'),
    chat_fill: require('../assets/icons/tabbar/chat_fill.png'),

    center_button: require('../assets/icons/tabbar/center_button.png'),

    home: require('../assets/icons/tabbar/home.png'),
    admin: require('../assets/icons/tabbar/admin.png'),
    wishlist: require('../assets/icons/tabbar/wishlist.png'),
    chat: require('../assets/icons/tabbar/chat.png'),


    heart_white: require('../assets/icons/heart_white.png'),
    heart_white_fill: require('../assets/icons/heart_white_fill.png'),


    get_started: require('../assets/images/onboarding/get_started.png'),
    onboarding_1: require('../assets/images/onboarding/onboarding_1.png'),
    onboarding_2: require('../assets/images/onboarding/onboarding_2.png'),
    onboarding_3: require('../assets/images/onboarding/onboarding_3.png'),

    view: require('../assets/images/view.png'),
    hide: require('../assets/images/hide.png'),


    dummy_avatar: require('../assets/images/dummy_avatar.png'),

    //empty
    no_chats: require('../assets/images/no_chats.png'),
    no_liked: require('../assets/images/no_liked.png'),
    no_ads: require('../assets/images/no_ads.png'),


    //categories
    bell: require('../assets/icons/categories/bell.png'),
    building: require('../assets/icons/categories/building.png'),
    bycicle: require('../assets/icons/categories/bycicle.png'),
    hobbies: require('../assets/icons/categories/hobbies.png'),

    clothes: require('../assets/icons/categories/clothes.png'),
    mobiles: require('../assets/icons/categories/mobiles.png'),
    car: require('../assets/icons/categories/car.png'),
    furniture: require('../assets/icons/categories/furniture.png'),
    jobs: require('../assets/icons/categories/jobs.png'),
    pet: require('../assets/icons/categories/pet.png'),

    electronics: require('../assets/icons/categories/electronics.png'),
    spareparts: require('../assets/icons/categories/spareparts.png'),


    // lottie
    success_lottie: require('../assets/lottie/success_lottie.json'),



}

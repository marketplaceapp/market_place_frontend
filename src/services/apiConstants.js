export const apiRequest = {
    get: 'get',
    patch: 'patch',
    post: 'post',
    put: 'put',
    delete: 'delete',
};
export const apiHeaders = {
    contentType: 'content-type',
    authorization: 'authorization',
    typeFormData: 'multipart/form-data',
    acceptType: 'application/json',
    deviceType: 'device-type',
    web: 'Web',
    klipAuthToken: 'klip-auth-token',
    language: 'language',
    timezone: 'timezone',
    today: 'today',
    appVersion: 'appversion',
    deviceDetails: 'devicedetails',
    deviceUdid: 'device-udid',
};

export const pathUrls = {
    
    //products
    products: '/products',

    get_ads: '/products/get_ads',
    get_owners_ads: '/products/get_owners_ads',

    delete_product: '/products/',
    

    add_product: '/products',
    get_products: '/products',
    get_products_by_id: '/products/',
    get_products_by_cat: '/products/category/',
    update_product: '/products/',
    update_product_images: '/products/images/',


    //categories
    categories: '/categories',
    

    //users
    users: '/users',
    user_login: '/auth/user/login',
    user_signup: '/auth/user/register',

    //admin login
    admin_login: '/admin/login',
    admin_signup: '/admin/register',
};

export const requestParams = {
    versionNo: 'versionNo',
    profilePicture: 'profilePicture',
    key: 'key',
    value: 'value',

    token: 'token',
    password: 'password',
    email: 'email',
    klipAuthToken: 'klip-auth-token',
    deviceType: 'device-type',
    web: 'Web',
    oldPassword: 'oldPassword',
    newPassword: 'newPassword',

    filter: 'filter',
    status: 'status',
    roles: 'roles',
    days: 'days',
    startDate: 'startDate',
    endDate: 'endDate',

    permissions: 'permissions',
    users: 'users',

    fullName: 'fullName',
    reason: 'reason',

    ids: 'ids',
    retailerId: 'retailerId',

    customerId: 'customerId',
    phone: 'phone',
    customerId: 'customerId',
    number: 'number',
    countryCode: 'countryCode',
    referenceno: 'referenceno',
    enableEncryption: 'enableEncryption',

    userTimeZone: 'userTimeZone',
    userTimeStamp: 'userTimeStamp',
    category: 'category',
    description: 'description',
    updatedAt: 'updatedAt',
    ticketId: 'ticketId',
    reply: 'reply',
    ticketIds: 'ticketIds',
};

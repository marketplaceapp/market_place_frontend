import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import CartScreen from '../../screens/dashboard/cart';

const Stack = createStackNavigator();

const CartStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Cart" 
        >
            <Stack.Screen
                name="Cart"
                component={CartScreen}
                options={{
                    headerShown: false,
                    // headerTitle: 'Home',
                    // headerLeft: () => null,
                    // headerTitleAlign: 'center',
                    // headerStyle: {backgroundColor: colors.black},
                    // headerTitleStyle: {color: colors.white, fontSize: fontSize.Title, fontFamily: Poppins.SemiBold},
                }}
            />
        </Stack.Navigator>
    );
}

export default CartStack
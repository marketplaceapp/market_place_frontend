import React, {useState, useEffect} from 'react'
import { Text, View, SafeAreaView, StyleSheet, TouchableOpacity, Image, ScrollView, Dimensions, useColorScheme, BackHandler, ImageBackground, } from 'react-native'

import { useDispatch, useSelector } from 'react-redux'

import { useNavigation } from '@react-navigation/native'
import LottieView from 'lottie-react-native'
// import { LocalNotification } from '../../utils/LocalPushController'
import { media } from '../../../global/media'
import { STRING_CONSTANTS, colors } from '../../../utils'

import { CustomHeader } from '../../../components/headers'
import { PrimaryButton } from '../../../components/button'

import { clearCartList } from '../../../store/modules/order/actions'
import AppText from '../../../components/text'
import { screenWidth } from '../../../global/constants'

const OrderConfirmation = (props) => {
    const dispatch = useDispatch();
    const navigation = useNavigation();

    useEffect(() => {
        // LocalNotification({
        //     title: "Order Placed Successfully! 🎉!",
        //     message: `Your product is on its way to you. Track your order now!`,
        // })
    
    }, [])
    
    const continueButton = () => {
        dispatch(clearCartList([]))
        navigation.navigate('HomeStack', {screen: 'Home'})
    }
    
    return (
        <SafeAreaView style={styles.container} >
            <ImageBackground source={media.bg_image} style={{flex: 1}} >
                    <CustomHeader title="Order Confirmation" />

                    <View style={styles.mainContainer} >
                        <Text></Text>
                        <View style={{alignItems: 'center'}} >
                            <LottieView
                                source={media.success_lottie} 
                                autoPlay 
                                loop 
                                style={{height: screenWidth-150, width: screenWidth-150, }}
                            />
                            <AppText
                                text={'SUCCESS!'}
                                type={STRING_CONSTANTS.textConstants.HEADLINE1}
                                customStyle={{color: colors.primary}}
                                numberOfLines={2}
                            />
                            <AppText
                                text={'Order has been placed!'}
                                type={STRING_CONSTANTS.textConstants.CAPTION1}
                                customStyle={{color: colors.primary}}
                                numberOfLines={2}
                            />
                        </View>

                        <PrimaryButton
                            title="Continue"
                            onPress={() => {continueButton()}}
                        />
                        
                    </View>
            </ImageBackground>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color,
    },
    mainContainer: {
        flex: 1,
        paddingHorizontal: 20,
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    // sccessTitle: {
    //     fontSize: 24,
    //     fontFamily: Poppins.Bold,
    //     color: '#FFFFFF',
    // },
    // sccessSubTitle: {
    //     fontSize: 20,
    //     fontFamily: Poppins.Medium,
    //     color: '#FFFFFF',
    // },
    
})


export default OrderConfirmation
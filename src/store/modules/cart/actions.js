import { 
    ADD_PRODUCTS_TO_CART, CLEAR_PRODUCTS_FROM_CART,
} from './actionTypes';


export function addProductsToCart(params) {
    return {
        type: ADD_PRODUCTS_TO_CART,
        payload: params
    };
};


export function clearProductsFromCart(params) {
    return {
        type: CLEAR_PRODUCTS_FROM_CART,
        payload: params
    };
};

import React, { createContext, useState } from 'react';
import {Text, View, Image, StyleSheet, } from 'react-native'

import { colors } from '../global/colors';
import { Poppins } from '../global/fontFamily';
import Icon from '../utils/icons';

export const ToastContext = createContext();

export const ToastProvider = ({ children }) => {
  const [title, setTitle] = useState('');
  const [subTitle, setSubTitle] = useState('');
  const [anchorOrigin, setAnchorOrigin] = useState({});
  const [isSuccess, setIsSuccess] = useState(true);

  const showToast = (title, isSuccess) => {
    setTitle(title || 'Error');
    setAnchorOrigin(anchorOrigin);
    setIsSuccess(isSuccess);
    setTimeout(() => {
      setTitle('');
      setSubTitle('');
    }, 3000);
  };

  return (
    <ToastContext.Provider value={{ showToast }}>
      {children}
      {title && (
        <View style={styles.container} >
            <View style={[styles.mainContainer, {backgroundColor: isSuccess ? colors.black : colors.white}, styles.shadowStyle]} >
                {isSuccess ?
                <Icon type="AntDesign" name="checkcircle" size={28} color={colors.white} />
                :
                <Icon type="AntDesign" name="exclamationcircle" size={28} color={colors.red} />
                }
                <Text numberOfLines={2} style={[styles.title, {color: isSuccess ? colors.white : colors.red}]} >{title}</Text>
            </View> 
        </View> 
      )}
    </ToastContext.Provider>
  );
};



const styles = StyleSheet.create({
    container: {
        position: 'absolute',
        bottom: 30,
        width: '100%',
    },
    mainContainer: {
        flex: 1,
        height: 55,
        borderRadius: 10,
        marginHorizontal: 20,
        paddingHorizontal: 15,
        borderWidth: 1,
        backgroundColor: colors.black,
        borderColor: colors.primary,
        alignItems: 'center',
        flexDirection: 'row',
    },
    title: {
        fontSize: 14,
        color: colors.white,
        fontFamily: Poppins.Medium,
        marginLeft: 14,
    },
    shadowStyle: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 2,
        },
        shadowOpacity: 0.34,
        shadowRadius: 1.27,
        elevation: 4,
    },
})







// import React, { createContext, useState } from 'react';
// import {Text, View, Image, StyleSheet, } from 'react-native'
// import Toast from '../components/toast';
// import { colors } from '../global/colors';
// import { Poppins } from '../global/fontFamily';
// import Icon from '../utils/icons';

// export const ToastContext = createContext();

// export const ToastProvider = ({ children }) => {
//   const [title, setTitle] = useState('');
//   const [subTitle, setSubTitle] = useState('');
//   const [isSuccess, setIsSuccess] = useState(true);

//   const showToast = (title, isSuccess) => {
//     setTitle(title);
//     setIsSuccess(isSuccess);
//     setTimeout(() => {
//       setTitle('');
//     }, 3000);
//   };

//   return (
//     <ToastContext.Provider value={{ showToast }}>
//       {children}
//       {title && (
//         <View style={styles.container} >
//             <View style={[styles.mainContainer, styles.shadowStyle]} >
//                 {isSuccess ?
//                 <Icon type="AntDesign" name="checkcircle" size={28} color={colors.white} />
//                 :
//                 <Icon type="AntDesign" name="exclamationcircle" size={28} color={colors.red} />
//                 }
//                 <Text numberOfLines={1} style={[styles.title, {color: isSuccess ? colors.white : colors.red}]} >{title}</Text>
//             </View> 
//         </View> 
//       )}
//     </ToastContext.Provider>
//   );
// };

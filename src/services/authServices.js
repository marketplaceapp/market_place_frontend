import AsyncStorage from '@react-native-async-storage/async-storage';

export const getUserFromLocalStorage = async () => {

    let user = await AsyncStorage.getItem('user');  

    if (!user) return null;

    let parsedUser = JSON.parse(user);
     return parsedUser;
};

export const getTokenFromLocalStorage = async () => {

    let auth_token = await AsyncStorage.getItem('auth_token');  

    if (!auth_token) return null;
    return auth_token;
};

export const removeUserFromLocalStorage = async () => {
    await AsyncStorage.removeItem('user');
    await AsyncStorage.removeItem('auth_token');
};

export const addUserToLocalStorage = async (user, token) => {
    console.log('user //  ', user);
    console.log('token //  ', token);
    const stringifiedUser = JSON.stringify(user)

    AsyncStorage.setItem('user', stringifiedUser);
    AsyncStorage.setItem('auth_token', token);
};

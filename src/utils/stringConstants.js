export default STRING_CONSTANTS = {
    textConstants: {
        LABEL: 'LABEL',
    
        LARGE_TITLE: 'LARGE_TITLE',
        TITLE1: 'TITLE1',
        TITLE2: 'TITLE2',
        TITLE3: 'TITLE3',
    
        HEADING: 'HEADING',
        HEADLINE1: 'HEADLINE1',
        HEADLINE2: 'HEADLINE2',
        HEADLINE3: 'HEADLINE3',
    
        SUBHEADLINE1: 'SUBHEADLINE1',
        SUBHEADLINE2: 'SUBHEADLINE2',
        SUBHEADLINE3: 'SUBHEADLINE3',
    
    
    
        SUBHEAD: 'SUBHEAD',
        CALLOUT: 'CALLOUT',
      
        CAPTION1: 'CAPTION1',
        CAPTION2: 'CAPTION2',
        CAPTION3: 'CAPTION3',
    
        BODY: 'BODY',
        TINY: 'TINY',

        ERROR: 'ERROR',
    },
    loginConstants: {
        email: 'Email',
        phonetext: 'Phone',
    },
    profileConstants: {
        dashboard: 'Dashboard',
        myOrders: 'My Orders',
        settings: 'Settings',
        helpCenter: 'Help Center',
        shareApp: 'Share App',
        logOut: 'Log Out',
    },
    language_english: 'en',
    language_arabic: 'ar',
    default_phone_country: 'United Arab Emirates',
    update_pin: 'update_pin',
    toast_type: {
        success: 'SUCCESS',
        error: 'ERROR',
    },
    HeaderOS: {
        android: 'AndroidApp',
        ios: 'IOSApp',
    },
    reset_pin2: 'reset_pin',
    delete_profile: 'delete_profile',
    expenses: 'expenses',
    chartType: {
        pie_chart: 'PieChart',
        arc: 'Arc',
    },
    first_banner: 'FIRST_BANNER',
    resolved: 'Resolved',
    pending: 'Pending',
    Active: 'Active',
}


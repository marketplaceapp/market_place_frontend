import React, { useState, useEffect } from 'react'
import { ActivityIndicator, Image, StyleSheet, Text, TextInput, TouchableOpacity, View } from 'react-native'
import { screenWidth } from '../../global/constants'
import { colors } from '../../global/colors'
import { Poppins } from '../../global/fontFamily'
import Loader from '../loader'
 

const LoginButton = ({title, disabled, buttonLoader, onPress}) => {
    return (
        <View style={styles.container} >
            <TouchableOpacity
                activeOpacity={0.5}
                style={[styles.button, {backgroundColor: disabled ? colors.gray : colors.black}]}
                onPress={onPress}
                disabled={buttonLoader ? true : disabled}
            >
                {buttonLoader ? <Loader/> : <Text style={styles.title} >{title}</Text>}
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        width: '100%',
        marginVertical: 14,
    },
    button: {
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        borderRadius: 10,
        backgroundColor: colors.primary,
    },
    title: {
        fontSize: 18,
        fontFamily: Poppins.SemiBold,
        color: colors.white,
    }
})

export default LoginButton
import React, { useDebugValue, useEffect, useState } from 'react'
import { Text, View, StyleSheet, Image, TouchableOpacity, SafeAreaView } from 'react-native'
import { STRING_CONSTANTS, colors } from '../../../utils'
import EmptyData from '../../../components/empty'
import { media } from '../../../global/media'
import { CustomHeader } from '../../../components/headers'
import { useNavigation } from '@react-navigation/native'
import AppText from '../../../components/text'
import CustomIcon from '../../../components/CustomIcon'
import { removeUserFromLocalStorage } from '../../../services/authServices'
import { useDispatch } from 'react-redux'
import { setAuthToken, userLoggedIn } from '../../../store/modules/auth/actions'
 
const SettingsScreen = (props) => {

    const dispatch = useDispatch()

    const navigation = useNavigation()

    const [details, setDetails] = useState({});

    useEffect(() => {
        const params = props?.route?.params?.params
        setDetails(params)
    }, [])
    
    const checkValue = async (val) => {
        console.log("dasd > ",val);

        if(val?.catTitle == 'Logout'){
            await removeUserFromLocalStorage()
            dispatch(userLoggedIn(false))
            dispatch(setAuthToken(null))
            navigation.reset({ index: 0,routes: [{ name: 'LoginStack' }]});
            //navigation.navigate('LoginStack')
        }
    }
    return (
        <SafeAreaView style={styles.container} >
            <CustomHeader
                title={details?.title}
                isBack={true}
            />

            <View style={styles.mainContainer}  >
                
            <View style={{width: '100%', paddingTop: 20}} >
                    {details && details?.sub_category?.map((item, index) => {
                        return(
                            <TouchableOpacity 
                            key={index}
                            activeOpacity={0.5}
                            onPress={() => {checkValue(item)}}
                            style={[styles.optionContainer, {borderBottomWidth: details?.sub_category?.length - 1 == index ? 0 : 1.5 }]} >
                            <View style={{flex: 1, alignItems: 'flex-start'}} >
                                <AppText
                                    text={item.catTitle}
                                    type={STRING_CONSTANTS.textConstants.CAPTION1}
                                    customStyle={{textAlign: 'left', color: colors.primary}}
                                />
                                {item.catSubTitle && (<AppText
                                    text={item.catSubTitle}
                                    type={STRING_CONSTANTS.textConstants.CAPTION2}
                                    customStyle={{textAlign: 'left', color: colors.primary}}
                                />)}
                            </View>
                            <CustomIcon
                                iconName="chevron-right"
                                iconType="Feather"
                                iconColor={colors.black}
                                iconContainer={{}}
                                iconSize={30}
                                //onPress={() => {navigation.navigate('Notifications')}}
                            />
                        </TouchableOpacity>
                        )
                    })}
                </View>
            </View>
        </SafeAreaView>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: colors.bg_color
    },
    mainContainer: {
        //flex: 1,
        padding: 20,
        paddingTop: 0,
        alignItems: 'center',
        justifyContent: 'center'
    },
    optionContainer: {
        width: '100%',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 14,
        borderColor: colors.light_gray
    },
})

export default SettingsScreen
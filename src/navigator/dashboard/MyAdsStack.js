import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import MyAdsScreen from '../../screens/dashboard/myAds';

const Stack = createStackNavigator();

const MyAdsStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="MyAds" 
        >
            <Stack.Screen
                name="MyAds"
                component={MyAdsScreen}
                options={{
                    headerShown: false,
                    // headerTitle: 'Home',
                    // headerLeft: () => null,
                    // headerTitleAlign: 'center',
                    // headerStyle: {backgroundColor: colors.black},
                    // headerTitleStyle: {color: colors.white, fontSize: fontSize.Title, fontFamily: Poppins.SemiBold},
                }}
            />
        </Stack.Navigator>
    );
}

export default MyAdsStack
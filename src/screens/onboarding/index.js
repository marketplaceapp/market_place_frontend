import React, { useState, useEffect } from 'react'
import { BackHandler, Image, SafeAreaView, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
 
import { onboarding_data } from '../../global/sampleData'
import AppText from '../../components/text'

import Icon from '../../utils/icons'
import { STRING_CONSTANTS, colors, fonts } from '../../utils'

const OnboardingScreen = ({navigation}) => {
    
    const [onboardingDetails, setOnboardingDetails] = useState(onboarding_data);
    const [currentIdx, setCurrentIdx] = useState(0);

    useEffect(() => {
        const backAction = () => {
            navigation.goBack()
            return true;
        };
    
        const backHandler = BackHandler.addEventListener(
            'hardwareBackPress',
            backAction,
        );
    
        return () => backHandler.remove();
    }, []);


    const nextButton = () => {
        if(currentIdx == onboardingDetails?.length - 1){
            navigation.navigate('Signup')
        }else{
            setCurrentIdx(prev => prev + 1)
        }
    }

    const prevButton = () => {
        setCurrentIdx(prev => prev - 1)
    }

    const Pagination = ({data, currentIdx}) => {
        return(
            <View style={styles.paginationContainer} >
                {data?.map((item, index) => (
                    <View style={currentIdx == index ? styles.paginationActive : styles.paginationInActive} >
                       
                    </View>
                ))}
            </View>
        )
    }

    return (
        <SafeAreaView style={styles.container} >
            <View style={styles.imageContainer} >
                <Image
                    source={onboardingDetails[currentIdx]?.image}
                    style={styles.image}
                />
            </View>
            <View style={styles.contentContainer} >
                <View>
                    <View style={{height: 70,}} >
                        <AppText
                            text={onboardingDetails[currentIdx]?.title}
                            type={STRING_CONSTANTS.textConstants.HEADING}
                            customStyle={{textAlign: 'center'}}
                        />
                    </View>
                    <AppText
                        text={onboardingDetails[currentIdx]?.description}
                        type={STRING_CONSTANTS.textConstants.BODY}
                        customStyle={{textAlign: 'center', marginVertical: 20}}
                    />
                </View>

                <View style={styles.buttonContainer} >
                    <View style={{width: 50,}} >
                        {currentIdx > 0 && (<TouchableOpacity
                            activeOpacity={0.5}
                            style={[styles.actionButtton, {backgroundColor: colors.white, borderColor: colors.primary, borderWidth: 1,}]}
                            onPress={() => {prevButton()}}
                        >
                            <Icon type="AntDesign" name="arrowleft" size={24} color={colors.primary} />
                        </TouchableOpacity>)}
                    </View>
                    
                    <Pagination
                        data={onboardingDetails}
                        currentIdx={currentIdx}
                    />
                    
                    <TouchableOpacity
                        activeOpacity={0.5}
                        style={styles.actionButtton}
                        onPress={() => {nextButton()}}
                    >
                        <Icon type="AntDesign" name="arrowright" size={24} color={colors.white} />
                    </TouchableOpacity>
                </View>
            </View>
        </SafeAreaView>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        width: '100%',
        alignItems: 'center',
        backgroundColor: colors.white,
        justifyContent: 'space-between'
    },
    imageContainer: {
        width: '100%',
        backgroundColor: colors.white,
    },
    contentContainer: {
        height: 300,
        width: '100%',
        padding: 25,
        position: 'absolute',
        alignItems: 'center',
        justifyContent: 'center',
        bottom: 0,
        borderTopRightRadius: 20,
        borderTopLeftRadius: 20,
        backgroundColor: colors.white,
    },
    buttonContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        width: '100%',
        marginTop: 20,
        justifyContent: 'space-between'
    },
    paginationContainer: {
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'center',
    },
    paginationActive: {
        height: 12, 
        width: 12,
        borderRadius: 6,
        marginHorizontal: 4,
        backgroundColor: colors.primary,
    },
    paginationInActive: {
        height: 10, 
        width: 10,
        borderRadius: 5,
        marginHorizontal: 4,
        backgroundColor: colors.gray,
    },
    actionButtton: {
        alignItems: 'center',
        justifyContent: 'center',
        width: 50,
        height: 50,
        borderRadius: 25,
        backgroundColor: colors.primary,
    },
    image: {
        width: '100%',
        height: 450,
        resizeMode: 'contain'
    },


    itemContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: colors.white,
    },
    text: {
        color: colors.white,
        fontSize: 24,
        textAlign: 'center',
        marginBottom: 20,
        fontFamily: fonts.SemiBold,
    },
    description: {
        color: colors.white,
        fontSize: 16,
        textAlign: 'center',
        fontFamily: fonts.Medium,
    }
  })
   
export default OnboardingScreen
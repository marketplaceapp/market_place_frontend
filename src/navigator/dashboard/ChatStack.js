import React from 'react';
import { createStackNavigator } from '@react-navigation/stack';

import ChatScreen from '../../screens/dashboard/chat';

const Stack = createStackNavigator();

const ChatStack = ({navigation}) => {


    return (
        <Stack.Navigator 
            initialRouteName="Chat" 
        >
            <Stack.Screen
                name="Chat"
                component={ChatScreen}
                options={{
                    headerShown: false,
                    // headerTitle: 'Home',
                    // headerLeft: () => null,
                    // headerTitleAlign: 'center',
                    // headerStyle: {backgroundColor: colors.black},
                    // headerTitleStyle: {color: colors.white, fontSize: fontSize.Title, fontFamily: Poppins.SemiBold},
                }}
            />
        </Stack.Navigator>
    );
}

export default ChatStack